## CICERO - Mission 1 - Decode Tux Rock Art
#

script()

   int waitTime
   waitTime = 2

   bool stateSet
   stateSet = false

   # Map where cicero is
   string barMap
   barMap = "mapas/tutorial/bar.map"

   # Cicero itself
   string cicero
   cicero = "characters/pcs/padre.pc"

   # define map where tux rock art is
   string map
   map = "mapas/tutorial/cave1.map"
   
   # Set Tux Rock Art object name and position
   string objName
   objName = "models/objetos/paints/tux_rock_art/tux_rock_art.dcc"
   float objPosX, objPosY, objPosZ
   objPosX = 495.831
   objPosY = -0.500
   objPosZ = 629.750

   int state
   int dialog

   while(true)
      
      if(!stateSet)
         if(ACTUAL_MAP == map)
            # Let's check rock art state
            state = getObjectState(getObject(objName, objPosX, objPosY, objPosZ))
            if(state != 0)
               stateSet = true
               # state -> 1 free software, 2 proprietary, 3 neutral
               # the cicero dialog according to this is state + 8.
               dialog = state + 8
               dialogSetInitial(cicero, barMap, dialog)
            end
        end
      end

      # Wait some time before next check.
      wait(waitTime)
   end

end

