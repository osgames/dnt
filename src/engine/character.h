/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_character_h
#define _dnt_character_h

#include <string>

#include "../gui/draw.h"
#include "../gui/picture.h"
#include "../gui/healthBar.h"
#include "../classes/thing.h"
#include "../classes/feats.h"
#include "../classes/classes.h"
#include "../classes/race.h"
#include "../classes/inventory.h"
#include "../classes/modifier.h"
#include "../ia/astar.h"
#include "../etc/animodel.h"
#include "../etc/list.h"
#include "../map/map.h"

#include "itemwindow.h"
#include "sun.h"
#include "cursor.h"

#define POSRETX 8   /**< X screen portrait position */
#define POSRETY 20  /**< Y screen portrait position */

#define MAX_DISTINCT_CLASSES 3 /**< Max Different Classes for MultiClass */

class engine;
class iaScript;

/*! Character Class */
class Character: public thing, public DntListElement
{
   public:
      /*! Constructor
       * \param ft -> pointer to all feats list
       * \param usedEngine -> pointer to the current engine */
      Character(featsList* ft, engine* usedEngine);
      /*! Destructor */
      ~Character();

      /*! Get all the attributes modifiers for the Character
       * \param mods -> vector that will contain the attribute modifiers */
      void getAttModifiers(int mods[6]);

      /*! Get the total level of the Character (AKA sum of all classes levels
       * the Character has.
       * \return total level of the Character */
      int getLevel();

      /*! Get the Character level for a specific class
       * \param classId -> id of the class
       * \return -> number of levels the Character has on class */
      int getLevel(std::string classId);

      /*! Get the Character level for a specific class
       * \param cl -> clas to get how many levels the Character has
       * \return -> number of lvels the Character has on class cl */
      int getLevel(classe* cl);

      /*! Get the current active (selected) feat for the Character
       * \return current active feat number */
      int getActiveFeat();

      /*! Get the current active feat pointer
       * \return pointer to the active feat */
      feat* getActiveFeatPtr();

      /*! Set the current Character active feat
       * \param f -> pointer to the feat to active */
      void setActiveFeat(feat* f);

      /*! Set the current Character active feat
       * \param f -> number of the feat to active */
      void setActiveFeat(int f);

      /*! Get the equiped Character weapon (if any)
       * \return poitner to the equiped weapon (or NULL) */
      weapon* getEquipedWeapon();

      /*! Verify if the Character alignment is of type represented by al
       * \param al -> string with align type
       * \return -> true if the align string identifier have the string al
       *            in it, false otherwise. 
       * \note -> a common use is, for example: isAlignOf("FREE_SOFTWARE") */
      bool isAlignOf(std::string al);

      /*! Define the image used as portrait
       * \param portraitFile -> file name of the image to use*/
      void definePortrait(std::string portraitFile);

      /*! Get the filename of the image used as portrait
       * \return -> file name of the image used */
      std::string getPortraitFileName();

      /*! Get the filename of the inventory defined to the Character
       * \return -> string with inventory file name or "" */
      std::string getInventoryFile();

      /*! Draw the Portrait */
      void drawMainPortrait();

      /*! Verify if the mouse is under portrait or not, setting text if is
       * \param mouseX -> mouse X coordinate
       * \param mouseY -> mouse Y coordinate */
      bool mouseUnderPortrait();
      /*! Verify if the mouse is under healthBar or not, setting text if is
       * \param mouseX -> mouse X coordinate
       * \param mouseY -> mouse Y coordinate */
      bool mouseUnderHealthBar();

      /*! Get First Level Skill Points
       * \param cl -> pointer to the class
       * \return -> total points at first level */
      int getFirstLevelSkillPoints(classe* cl);

      /*! Get Other Levels Skill Points
       * \param cl -> pointer to the class
       * \return -> total points at first level */
      int getOtherLevelSkillPoints(classe* cl);

      /*! Define Character initial life points, based on its class */
      void defineInitialLifePoints();

      /*! Update the health bar draw */
      void updateHealthBar();

      /*! Verify if the Character can take levels on a specific class
       * \param cl -> class to verify if can take levels 
       * \return -> true if can take a level, false otherwise */
      bool canClass(classe* cl);

      /*! Get a new level at the class
       * \param cl -> pointer to the class to get a new level
       * \note -> will only take a level if have enought XP
       *          and can take a level at the specified class */
      void getNewClassLevel(classe* cl);

      /*! Verify if a Character can have a feat or not (based on its
       * pre-requisites)
       * \param f -> pointer to the featDescription to verify
       * \return true if can  */
      bool canHaveFeat(featDescription* f);

      /*! Verify if already have a feat
       * \param featId -> identifier of the feat
       * \return true if already have the feat */
      bool haveFeat(std::string featId);

      /*! Clear Skills */
      void clearSkills();

      /*! Set Character orientation value
       * \param ori -> new orientation value */
      void setOrientation(GLfloat ori);

      /*! Kill the Character without calling dead animation
       * \note this is usually used at modState  */
      void instantKill();

      /*! Call the thing dead animation */
      void callDeadAnimation();

      /*! Call the thing attack animation */
      void callAttackAnimation();

      /*! Call the thing attack animation */
      void callIdleAnimation();

      /*! Call a one cycle animation action */
      void callActionAnimation(int id);
      /*! Set animation state (repeated) */
      void callAnimation(int id);

      /*! Delete the Existed Inventory and Create a new One. 
       *  Usually called after death*/
      void newInventory();

      /*! Apply all Race and Classes Skills Costs */
      void applySkillCosts();

      /*! Apply Bonus And Saves from Classes to the Character */
      void applyBonusAndSaves();

      /*! Get the Character filename
       * \return -> the Character filename */
      std::string getCharacterFile(){return(CharacterFile);};

      /*! Set the Character file
       * \param fileName -> new fileName of the Character */
      void setCharacterFile(std::string fileName){CharacterFile = fileName;}

      /*! Save the Character to a new file 
       * \param fileName -> name of the file to save
       * \return-> true if can save, false otherwise */
      bool save(std::string fileName);

      /*! Get the active attack feat range
       * \return range value in meters */ 
      int getActiveFeatRange();

      /*! Insert the default needed feats (for example: base attack feat)
       * \param ft -> pointer to all available feats on game */
      void insertDefaultNeededFeats(featsList* ft);

      /*! Get the general script pointer
       * \return genereal iaScript pointer */
      iaScript* getGeneralScript();

      /*! Call the script when Character is killed */
      void callKilledScript();

      /*! Add a modEffect to the Character
       * \param mod -> modifier value
       * \param time -> time to expire (0 == forever)
       * \param periodicTime -> time to apply the modEffect again 
       *                        (0 to not re-apply)
       * \param factorId -> id of the target factor
       * \param factorType -> type of the target factor */
      void addModEffect(int mod, int time, int periodicTime,
                        std::string factorId, std::string factorType);

      /*! Remove all modEffects from the Character */
      void removeAllModEffects();

      /*! Get the modEffectList pointer
       * \return pointer to the modEffectList */
      modEffectList* getEffects();

      /*! Define the current occuped square by the Character
       * \param curMap -> pointer to the current map opened */
      void defineOcSquare(Map* curMap);

      /*! Update all effects affecting the Character */
      void updateEffects();

      /*! Get the damage dice of Character 'empty hands'
       * \return bared hands damage dice */
      diceThing getBaredHandsDice();

      /*! Define the Character list as friend class */
      friend class CharacterList;

      classe* actualClass[MAX_DISTINCT_CLASSES]; /**< Pointer to each class */
      int classLevels[MAX_DISTINCT_CLASSES]; /**< Current level of each class */
      race* actualRace;         /**< Pointer to Race */
      align* actualAlign;       /**< Pointer to Align */
      feats* actualFeats;       /**< Feats owned by Character */      

      int actualFightGroup;     /**< fightGroup of Character, used on battles*/

      Farso::HealthBar* lifeBar;       /**< Character's Life Bar */

      aStar pathFind;           /**< The A* pathFind to the Character */
      Square* ocSquare;         /**< Square occuped by Character */

      Inventory* inventories;   /**< Inventory */

   protected:
      /*! Get points based on calculation
       * \param pt -> calculation info
       * \return points */
      int getPoints(points pt);

      /*! Blit level up image to portrait texture */
      void blitLevelUpImage();

      std::string talkPortrait;        /**< Portrait talk file name */
      std::string CharacterFile;       /**< Name of the Character file */
      float portraitPropX;        /**< X proportion for portrait texture */
      float portraitPropY;        /**< Y proportion for portrait texture */
      SDL_Surface* portraitImage; /**< Character's portrait image */
      bool showLevelUp;           /**< If levelUp image is displayed or not */
      SDL_Surface* levelUpImage;  /**< Level up image to be on portrait */
      GLuint portraitTexture;     /**< The Portrait Texture */
      std::string inventoryFile;       /**< The inventory file name */

      int activeFeat;             /**< The Character active feat */
      
      diceThing bareHandsDice;    /**< Damage by bare hands */

      modEffectList* effects;     /**< Current Character effects */

      engine* curEngine;          /**< current used engine */

      iaScript* generalScript;    /**< Pointer to the general iaScript */
      std::string generalScriptFileName; /**< The General Script Filename */
      iaScript* killedScript;     /**< The Killed script */
      std::string killedScriptFileName; /**< Script when killed */
      DNT::Mouse mouse; /**< mouse used */
};


/*! Character's List */
class CharacterList: public DntList
{
   public:
      /*! List Constructor */
      CharacterList();
      /*! List Destructor */
      ~CharacterList();

      /*!
       * Insert one Character on list
       * \param file -> file name of the Character to insert
       * \param ft -> featsList of all feats on game 
       * \param pEngine -> pointer to current engine
       * \param curMap -> fileName of the current map
       * \return pointer to opened Character*/
      Character* insertCharacter(std::string file, featsList* ft, 
                                 engine* pEngine, std::string curMap);

      /*!
       * Remove one Character from list
       * \param persona -> Character pointer to remove */
      void removeCharacter(Character* dude);

      /*!
       * Gets hostile Characters from the list
       * \param last -> last hostile taken
       * \return pointer to the hostile Character
       */
      Character* getEnemyCharacter(Character* last);

      /*! Get the active Character
       * \return pointer to the active Character */
      Character* getActiveCharacter();

      /*! Get the first Character with fileName on the list
       * \param fileName -> fileName of the Character to get
       * \return -> characte pointer or NULL */
      Character* getCharacter(std::string fileName);

      /*! Get the Character related to the SceneNode
       * \return Character pointer or NULL */
      Character* getCharacter(SceneNode* scNode);

      /*! Get the next Character with the same model from the list
       * \param ch -> Character to get next witg same model
       * \return -> next Character with same model or NULL if
       *            end of the list was reached. */
      Character* getNextSameCharacter(Character* ch);

      /*! Set the active Character
       * \param dude -> pointer to the new active Character */
      void setActiveCharacter(Character* dude);

      /*! Treat Character's general scripts
       * \param actualMap -> current opened map
       * \param NPCs -> current NPCs list  */
      void treatGeneralScripts(Map* actualMap, CharacterList* NPCs);

      /*! Verify if the Character is in the list
       * \paarm ch -> pointer to the Character
       * \return -> true if the Character is in the list */
      bool isCharacterIn(Character* ch);

      /*! Update effects and positions of all Characters influences */
      void update();

   private:

      Character* activeCharacter;  /**< Active Character's on list */
      Character* curTreat;         /**< Current NPC To treat Scripts */

};

#endif

