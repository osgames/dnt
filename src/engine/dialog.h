/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_dialog_h
#define _dnt_dialog_h

/*! \file dialog.h Define Conversations options.*/

#define MAX_OPTIONS 5 /**< Max number of options per dialog */

#define MAX_ACTIONS 8 /**< Max number of actions per option */

#define MAX_PRE_TESTS 2 /**< Max number of preTests per option */

#include "../gui/farso.h"
#include "../etc/list.h"

#include "character.h"
#include <string>

/*! TalkAction Struct */
class TalkAction
{
  public:

     /*! Constructor */
     TalkAction();

     int id;   /**< Talk Action Identificator */
     int oper; /**< Operator used */
     int qty;  /**< Quantity */
     int att;  /**< Attribute to modify */
     std::string satt; /**< String Attribute */
};

/* a talk test */
class talkTest
{
   public:

      /*! Constructor  */
      talkTest();

      /*! Set the Talk test function
       * \param token -> function token string
       * \param t -> the test string
       * \param a -> the against string */
      bool set(std::string token, std::string t, std::string a);

      /*! Do the test with a Character
       * \param pc -> pointer to the Character to test
       * \param owner -> the owner of the dialog (object or Character)
       * \return -> true if test pass */
      bool doTest(Character* pc, thing* owner);

      /*! Get the test name (ie. if a skill, the skill name)
       * \param pc -> pointer to the Character
       * \return -> the test name */
      std::string getTestName(Character* pc);

   protected:

      /*! Get the against value (or skill value) */
      int getAgainstValue(thing* owner);

      int id;            /**< Talk test ID */
      std::string test;       /**< The modifier to test */
      std::string against;    /**< The against modifier to test (or value) */
};

/*! The dialog Option definition */
class DialogOption
{
   public:

      /*! Constructor */
      DialogOption();       

      talkTest preTest[MAX_PRE_TESTS]; /**< The pre test 
                                            (if all true, the option is show) */

      std::string text;    /**< The option text */

      talkTest postTest;   /**< The post test (if true run ifActions) */

      TalkAction ifAction[MAX_ACTIONS];   /**< If postTest == true actions */
      int totalIfActions;                 /**< Number of if actions */
      TalkAction elseAction[MAX_ACTIONS]; /**< If postTest == false actions */
      int totalElseActions;               /**< Number of else actions */
};

/*! Dialog Struct */
class Dialog: public DntListElement
{
   public:
      std::string npcText;                /**< NPC text */
      DialogOption options[MAX_OPTIONS];  /**< PC options to talk */
      int id;                             /**< Identificator */
};

class engine;

/*! Conversation Class. A Conversation is a set of dialogs, usually for
 * a NPC Character or an object. */
class Conversation: public DntList
{
   public:
      /*! Conversation Constructor */
      Conversation();
      /*! Conversation destructor */
      ~Conversation();

      /*!
       * Load file that contents the dialog.
       * \param name -> file name to load
       * \return 1 if succed. */
      int loadFile(std::string name);  

      /*!
       * Save dialog file
       * \param name -> file name to save
       * \return 1 if succed. */
      int saveFile(std::string name);    

      /*!
       * Insert dialog on Conversation
       * \return pointer to dialog */
      Dialog* insertDialog();        

      /*!
       * Delete dialog from Conversation
       * \param num -> number of the dialog to be removed */
      void removeDialog(int num); 

      /*! Set the initial dialog (the one displayed at a click on Character)
       * \param numDialog -> new init dialog number */
      void setInitialDialog(int numDialog);

      /*! Set the owner of the Conversation
       * \param thing -> pointer to thing owner
       * \param mapFile -> fileName where the NPC is */
      void setOwner(thing* o, std::string mapFile);

      /*! Set the playable Character pointer
       * \param PC -> pointer to the playable Character */
      void setPC(Character* PC);

      /*! Get the owner of the dialog (if one)
       * \return -> pointer to the owner or NULL */
      thing* getOwner();

      /*! Get the current PC talking to the NPC
       * \return -> pointer to the PC Character */
      Character* getPC();

      /*! Computates the action on dialog, based on selected option.
       * \param opcao -> option selected 
       * \param curEngine -> pointer to the current engine */
      void proccessAction(int opcao, engine* curEngine);

      /*! Change dialog
       * \param numDialog -> number of the new dialog to use */
      void changeDialog(int numDialog);

      /*! Change dialog to the initial one */
      void changeDialog();

   protected:

      /*! Get the dialog pointer
       * \param id -> id of the dialog to get
       * \return pointer to the dialog or NULL */
      Dialog* getDialog(int id);

      int actual;           /**< Actual active Dialog */
      int initialDialog;    /**< First dialog to show */
      Character* actualPC;  /**< The Actual PC */
      thing* owner;         /**< The owner of the Conversation */
      std::string ownerMap;      /**< The Map Owner */
      
      std::string getString(int& initialPosition, std::string buffer, 
                            char& separator);
      int getActionID(std::string token, std::string fileName, int line);
      void printError(std::string fileName, std::string error, int lineNumber);
};

/*! The window where will show a dialog */
class DialogWindow
{
   public:
      /*! Open the dialog window
       * \param PC -> player's Character 
       * \param cv -> pointer to the Conversation to show 
       * \param pictureFile -> filename of the picture to use */
      void open(Character* PC, Conversation* cv, std::string pictureFile);

      /*! Treat Events on Window. 
       * \param guiObj -> active GUI object
       * \param eventInfo -> last GUI Event 
       * \param infoW -> pointer to the used ItemWindow
       * \param curEngine ->pointer to the current engine
       * \return true if event is threated, false otherwise. */
      bool treat(Farso::GuiObject* guiObj, int eventInfo,
                 ItemWindow* infoW, engine* curEngine);

      /*! Verify if the dialog is open or not 
       * \return true if the window is opened */
      bool isOpened();

      /*! Verify if the dialog window is opened for an specific Conversation
       * \param cv -> pointer to the Conversation to verify if the 
       *              dialog window is opened for
       * \return -> true if the dialog window is opened for this Conversation
       *            false if the dialog window is closed or opened for another 
       *            Conversation. */
      bool isOpened(Conversation* cv);

      /*! Close, if opened, the dialog window */
      void close();

      /*! Redraw the window */
      void redraw();

      /*! Set the current NPC Text
       * \param text -> current npc text to display */
      void setNPCText(std::string text);

      /*! Clear all options */
      void clearOptions();

      /*! Add a option 
       * \param optNumber -> the option number
       * \param text -> the option text 
       * \param info -> the option info */
      void addOption(int optNumber, std::string text, int info);

   protected:
      static Conversation* conv;     /**< Pointer to the Conversation used */
      static Farso::Window* jan;         /**< Pointer to window used to show */
      static Farso::Farso farso;  /**< Pointer to the used interface */
      static Farso::RolBar* npcText;        /**< The NPC text quad */
      static Farso::SelText* pcSelText;     /**< The PC selection text */
      static Farso::Button* barterButton;   /**< The Barter Button */
      static int pressKey;              /**< Key pressed for dialog selection */
      static Farso::Keyboard keyboard;
};

#endif

