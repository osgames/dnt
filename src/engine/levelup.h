/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_level_up_h
#define _dnt_level_up_h

#include "character.h"
#include "../gui/farso.h"

/*! The levelUp class is used to level up Characters when its
 * experience points reached enought level.
 * \note Will receive a new talent every time totaLevels() % 5 == 0 or == 3 */
class levelUp
{
   public:
      /*! Constructor
       * \param c -> pointer to Character to level up
       * \param ft -> pointer to the current talents on game */
      levelUp(Character* c, featsList* ft);
      /*! Destructor */
      ~levelUp();

      /*! Do the level Up process (calling the needed windows, etc) */
      void doLevelUp();

   protected:
      Farso::Farso farso;
      Character* current;
      featsList* features;
      int state;
};

#endif

