/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_cursor_h
#define _dnt_cursor_h

#include <string>

#include "../gui/mouse.h"

namespace DNT
{

enum
{
   CURSOR_WALK  =0,   /**< Walk Mouse Cursor */
   CURSOR_ATTACK,     /**< Attack Mouse Cursor */
   CURSOR_DEFEND,     /**< Defend Mouse Cursor */
   CURSOR_MAPTRAVEL,  /**< Map Travel Mouse Cursor */
   CURSOR_TALK,       /**< Talk Mouse Cursor */
   CURSOR_GET,        /**< Get Mouse Cursor */
   CURSOR_INVENTORY,  /**< Inventory Mouse Cursor */
   CURSOR_DOOR,       /**< Door Mouse Cursor */
   CURSOR_FORBIDDEN,  /**< Forbidden Mouse Cursor */
   CURSOR_WALK_CONT,  /**< Continuous Walk Cursor */
   CURSOR_USE,        /**< Object Use */
   CURSOR_TOTAL,      /**< Total number of mouse cursors */
   CURSOR_USER_IMAGE  /**< The special User Image cursor */
};

/*! The DNT mouse cursor class.*/
class Mouse : public Farso::MouseCursor
{
   public:
      /*! Set current mouse cursor, by DNT cursor constant.
       * \param nCursor -> cursor number to use. */
      void set(int nCursor);

      /*! Set the current mouse Cursor to an image
       * \param img -> sdl surface to set as cursor */
      void set(SDL_Surface* img);

      /*! Get current mouse cursor constant */
      int get();

   private:
      /*! Preload all cursors to use */
      void loadAll();

      /*! Each cursor image, indexed by DNT cursor constant */
      static const std::string cursorFile[];

      /*! Each cursor hotspot, indexed by DNT cursor constant */
      static const int cursorHotSpot[][2];

      static int currentCursor; /**< Current Cursor Index */
      static bool loadedAll; /**<  If loaded all cursors */
      
};

}

#endif

