/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "barterwindow.h"
#include "cursor.h"
#include "../etc/dirs.h"
#include "../sound/sound.h"
#include "dccnit.h"

/***************************************************************
 *                         Constructor                         *
 ***************************************************************/ 
BarterWindow::BarterWindow()
{
}

/***************************************************************
 *                             open                            *
 ***************************************************************/
void BarterWindow::open(Character* s, Character* b, ItemWindow* infoW, 
                        engine* usedEngine)
{
   dirs dir;
   /* Init Values */
   infoWindow = infoW;
   curEngine = usedEngine;

   int midX = Farso::SCREEN_X / 2;

   /* Set Characters */
   seller = s;
   buyer = b;

   /* Create Barter */
   barterInventory = new barter(seller, buyer);
   
   /* Create Window */
   intWindow = farso.getMainGuiInterface()->insertWindow(midX - 132, 0,
         midX + 132, 288, gettext("Barter"));

   /* Totals */
   buyerTotals = intWindow->getObjectsList()->insertTextBox(30,236,128,252,
                                                            0,"0,00");
   sellerTotals = intWindow->getObjectsList()->insertTextBox(162,236,251,252,
                                                             0,"0,00");
   buyerTotals->setColor(255,10,10);
   sellerTotals->setColor(255,10,10);

   /* Actions Buttons */
   offerButton = intWindow->getObjectsList()->insertButton(173,262,251,280,
                                             gettext("Offer"),0);
   imposeButton = intWindow->getObjectsList()->insertButton(91,262,168,280,
                                             gettext("Impose"),0);
   cancelButton = intWindow->getObjectsList()->insertButton(9,262,87,280,
                                             gettext("Cancel"),0);

   /* Money Images */
   intWindow->getObjectsList()->insertPicture(4,234,0,0,
                       dir.getRealFile("texturas/inventory/moeda.png").c_str());
   intWindow->getObjectsList()->insertPicture(136,234,0,0,
                       dir.getRealFile("texturas/inventory/moeda.png").c_str());


   /* Create the TabButton */
   barterTabButton = intWindow->getObjectsList()->insertTabButton(7,41,256,220,
                       dir.getRealFile("texturas/inventory/trade.png").c_str());

   /* Buyer Buttons */
   buyerInv = barterTabButton->insertButton(0,0,114,190);

   /* Seller Buttons */
   sellerInv = barterTabButton->insertButton(133,0,247,190);

   /* Open Window */
   intWindow->setExternPointer(&intWindow);
   farso.getMainGuiInterface()->openWindow(intWindow);

   /* Verify if the buyer and seller windows are opened. If not open them */
   if(seller->inventories->getOpenedWindow() == NULL)
   {
      sellerWindow = new InventWindow(midX+133,0,gettext("Inventory"),
                                      seller, infoWindow, curEngine);
   }
   else
   {
      sellerWindow = NULL;
   }

   if(buyer->inventories->getOpenedWindow() == NULL)
   {
      buyerWindow = new InventWindow(midX-400,0,gettext("Inventory"),
                                     buyer, infoWindow, curEngine);
   }
   else
   {
      buyerWindow = NULL;
   }
}

/***************************************************************
 *                          Destructor                         *
 ***************************************************************/
BarterWindow::~BarterWindow()
{
}

/***************************************************************
 *                            close                            *
 ***************************************************************/
void BarterWindow::close()
{
   /* Cancel the barter if the window is opened */
   if(isOpen())
   {
      cancel();
      farso.getMainGuiInterface()->closeWindow(intWindow);
      intWindow = NULL;
   }

   /* Close the Inventory windows, if them are opened here. */
   if(buyerWindow)
   {
      delete(buyerWindow);
      buyerWindow = NULL;
   }
   if(sellerWindow)
   {
      delete(sellerWindow);
      sellerWindow = NULL;
   }

   /* Delete the barter inventory */
   if(barterInventory)
   {
      delete(barterInventory);
      barterInventory = NULL;
   }
}

/**************************************************************
 *                            isOpen                          *
 **************************************************************/
bool BarterWindow::isOpen()
{
   return(intWindow != NULL);
}

/**************************************************************
 *                         addSellItem                        *
 **************************************************************/
bool BarterWindow::addSellItem(object* obj)
{
   bool result = false;
   if(barterInventory)
   {
      result = barterInventory->addSellItem(obj);
      reDraw();
   }
   return(result);
}

/**************************************************************
 *                          addBuyItem                        *
 **************************************************************/
bool BarterWindow::addBuyItem(object* obj)
{
   bool result = false;
   if(barterInventory)
   {
      result = barterInventory->addBuyItem(obj);
      reDraw();
   }
   return(result);
}

/**************************************************************
 *                       removeSellItem                       *
 **************************************************************/
void BarterWindow::removeSellItem(int x, int y, int curSellSlot)
{
   if(barterInventory)
   {
      barterInventory->removeSellItem(x,y, curSellSlot);
      reDraw();
   }
}

/**************************************************************
 *                       removeBuyItem                        *
 **************************************************************/
void BarterWindow::removeBuyItem(int x, int y, int curBuySlot)
{
   if(barterInventory)
   {
      barterInventory->removeBuyItem(x,y, curBuySlot);
      reDraw();
   }
}

/**************************************************************
 *                            reDraw                          *
 **************************************************************/
void BarterWindow::reDraw()
{
   char value[32];
   if(isOpen())
   {
      /* Draw the barter inventory */
      barterInventory->draw(0,0, barterTabButton->get(), 
                            0, 0);

      /* Draw the Prices Text */

      /* The PC (buyer) is selling to the seller */
      sprintf(value, "%.2f", barterInventory->getTotalSellValue());
      buyerTotals->setText(value);

      /* The PC is buying from the seller */
      sprintf(value, "%.2f", barterInventory->getTotalBuyValue());
      sellerTotals->setText(value);

      intWindow->draw(0,0);
   }
}

/**************************************************************
 *                            cancel                          *
 **************************************************************/
void BarterWindow::cancel()
{
   barterInventory->cancelBarter();
}

/**************************************************************
 *                             offer                          *
 **************************************************************/
bool BarterWindow::offer()
{
   bool res = (barterInventory->doBarter());
   Briefing brief;

   if(res)
   {
      /* The barter was accepted, so close all barter related windows */
      close();
      brief.addText(gettext("The barter was accepted."),67,92,215);
   }
   else
   {
      /* Tell the user it was rejected */
      brief.addText(gettext("The barter was rejected."),190,13,18);
   }

   return(res);
}

/**************************************************************
 *                            impose                          *
 **************************************************************/
bool BarterWindow::impose()
{
   Briefing brief;
   bool res = barterInventory->imposeBarter();

   //FIXME -> enter battle mode

   if(res)
   {
      /* The imposition was accepted, so close all barter related windows */
      close();
      brief.addText(gettext("The barter imposition was accepted."),67,92,215);
   }
   else
   {
      /* Tell the user it was rejected */
      brief.addText(gettext("The barter imposition was rejected."),190,13,18);
   }

   return(res);
}

/**************************************************************
 *                           openMenu                         *
 **************************************************************/
void BarterWindow::openMenu(int mouseX, int mouseY)
{
   int x = mouseX - intWindow->getX1();
   int y = mouseY - intWindow->getY1();
   int xSize = 0;
   dirs dir;
   Farso::Font fnt;

   /* Create the Menu */
   objectMenu = (Farso::Menu*) intWindow->getObjectsList()->addMenu();
   objectMenu->insertItem(gettext("Remove"), dir.getRealFile("icons/drop.png"), 
                          true);

   fnt.defineFont(dir.getRealFile(DNT_FONT_ARIAL),12);

   xSize = objectMenu->getMaxCharac()*(fnt.getIncCP()+1)+6;

   /* Make Sure all Menu is in Window */
   if( (y + 20) >= (intWindow->getY2() - intWindow->getY1()) )
   {
      y = intWindow->getY2()-intWindow->getY1()-20;
   }

   if( (x + xSize) >= (intWindow->getX2() - intWindow->getX1()) )
   {
      x = intWindow->getX2() - intWindow->getX1()  - xSize;
   }

   objectMenu->setPosition(x,y);

   sound snd;
   snd.addSoundEffect(SOUND_NO_LOOP, "sndfx/gui/zipclick-press.ogg");
}

/**************************************************************
 *                            treat                           *
 **************************************************************/
bool BarterWindow::treat(Farso::GuiObject* guiObj, int eventInfo, 
      Map* actualMap)
{
   bool res = false;
   int posX = -1;
   int posY = -1;

   if(!isOpen() && (barterInventory != NULL))
   {
      /* No more window, so free the structures! */
      close();
      return(true);
   }
   else if(!isOpen())
   {
      /* Nothing to do here */
      return(false);
   }

   /* Treat Inventory windows openned here */
   if(buyerWindow)
   {
      buyerWindow->treat(guiObj, eventInfo, actualMap, 
                         buyer->scNode->getPosX(), buyer->scNode->getPosZ());
   }
   if(sellerWindow)
   {
      sellerWindow->treat(guiObj, eventInfo, actualMap, 
                          seller->scNode->getPosX(), seller->scNode->getPosZ(),
                          true);
   }

   /* Gather GUI events */
   switch(eventInfo)
   {
      case Farso::EVENT_PRESSED_BUTTON:
      {
         /* Cancel the Barter */
         if(guiObj == (Farso::GuiObject*) cancelButton)
         {
            cancel();
            close();
            res = true;
         }
         /* Impose Barter */
         else if(guiObj == (Farso::GuiObject*) imposeButton)
         {
            impose();
            res = true;
         }
         /* Offer Barter */
         else if(guiObj == (Farso::GuiObject*) offerButton)
         {
            offer();
            res = true;
         }
      }
      break;

      case Farso::EVENT_PRESSED_TAB_BUTTON:
      case Farso::EVENT_ON_FOCUS_TAB_BUTTON:
      {
         /* Inventory Spaces Selected */
         if( (guiObj == (Farso::GuiObject*) buyerInv) || 
             (guiObj == (Farso::GuiObject*) sellerInv) )
         {
            /* Define Inventory */
            sellerObj = (guiObj == (Farso::GuiObject*) sellerInv);
            int curInv = 0;

            /* Convert Mouse Coordinate to inventory coordinate */
            if(sellerObj)
            {
               posX = (int) floor((mouse.getX() - 
                                  (140 + intWindow->getX1())) / 19.0);
               posY = (int) floor((mouse.getY() - 
                                  (41 + intWindow->getY1())) / 19.0);
            }
            else
            {
               posX = (int) floor((mouse.getX() - 
                                  (7 + intWindow->getX1())) / 19.0);
               posY = (int) floor((mouse.getY() - 
                                  (41 + intWindow->getY1())) / 19.0);
            }

            /* Open Menu For Object if one is avaible */
            activeObject = barterInventory->getFromPosition(posX, posY, 
                                                            curInv, sellerObj);
            if(activeObject)
            {
               activeObject = barterInventory->getFromPosition(posX, posY,
                     curInv, sellerObj);

               if(eventInfo == Farso::EVENT_PRESSED_TAB_BUTTON)
               {
                  objX = posX;
                  objY = posY;

                  openMenu(mouse.getX(), mouse.getY());
               }
               else
               {
                  mouse.setTextOver(activeObject->name);
               }
            }
            res = true;
         }
      }
      break;

      case Farso::EVENT_SELECTED_MENU:
      {
         if( (objectMenu) && (activeObject))
         {
            switch(objectMenu->getActualItem())
            {
               case 1: /* Remove */
               {
                  if(sellerObj)
                  {
                     removeBuyItem(objX, objY, 0);
                  }
                  else
                  {
                     removeSellItem(objX, objY, 0);
                  }
                  reDraw();
               }
               break;
            }
            intWindow->getObjectsList()->removeMenu();
            objectMenu = NULL;
            res = true;
         }
      }
      break;
   }

   return(res);
}



/**********************************************************************
 *                            Static Members                          *
 **********************************************************************/
Farso::Farso BarterWindow::farso;
Character* BarterWindow::buyer = NULL;
Character* BarterWindow::seller = NULL;

object* BarterWindow::activeObject = NULL;
int BarterWindow::objX = 0;
int BarterWindow::objY = 0;
bool BarterWindow::sellerObj = false;

barter* BarterWindow::barterInventory = NULL;
Farso::TextBox* BarterWindow::sellerTotals = NULL;
Farso::TextBox* BarterWindow::buyerTotals = NULL;
Farso::Window* BarterWindow::intWindow = NULL;
Farso::Button* BarterWindow::imposeButton = NULL;
Farso::Button* BarterWindow::offerButton = NULL;
Farso::Button* BarterWindow::cancelButton = NULL;

Farso::TabButton* BarterWindow::barterTabButton = NULL;
Farso::OneTabButton* BarterWindow::sellerInv = NULL;
InventWindow* BarterWindow::sellerWindow = NULL;

Farso::OneTabButton* BarterWindow::buyerInv = NULL;
InventWindow* BarterWindow::buyerWindow = NULL;

Farso::Menu* BarterWindow::objectMenu = NULL;

ItemWindow* BarterWindow::infoWindow = NULL;
engine* BarterWindow::curEngine = NULL;
DNT::Mouse BarterWindow::mouse;

