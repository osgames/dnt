/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "savewindow.h"

#ifdef __APPLE__
   #include <SDL_image/SDL_image.h>
#else
   #include <SDL2/SDL_image.h>
#endif

#include "cursor.h"
#include "options.h"
#include "util.h"

#include "../etc/userinfo.h"
#include "../lang/translate.h"
using namespace std;

/***********************************************************************
 *                             Constructor                             *
 ***********************************************************************/
SaveWindow::SaveWindow()
{
   curFileName = "";
}

/***********************************************************************
 *                              Destructor                             *
 ***********************************************************************/
SaveWindow::~SaveWindow()
{
}

/***********************************************************************
 *                          getSelectedFileName                        *
 ***********************************************************************/
string SaveWindow::getSelectedFileName()
{
   return(curFileName);
}

/***********************************************************************
 *                                open                                 *
 ***********************************************************************/
void SaveWindow::open()
{
   int midX=0, midY=0;
   int dX=0, dY=0;
   userInfo info;

   /* Define Sizes and positions */
   midX = (Farso::SCREEN_X / 2);
   midY = (Farso::SCREEN_Y / 2);
   if(windowIsLoad)
   {
      dX = 132;
      dY = 140;
   }
   else
   {
      dX = 132;
      dY = 95;
   }

   /* Create the window */
   fileWindow = gui->insertWindow(midX-dX, midY-dY, midX+dX, midY+dY,
                                  windowIsLoad?gettext("Load"):gettext("Save"));

   /* Set the File Selector and Image/Text, if needed */
   if(windowIsLoad)
   {
      fileImage = fileWindow->getObjectsList()->insertPicture(8,18,0,0,NULL);
      fileWindow->getObjectsList()->insertTextBox(6,16,137,115,2,"");
      fileTitle = fileWindow->getObjectsList()->insertTextBox(138,16,256,115,
                                                              2,"");
      fileSelector = fileWindow->getObjectsList()->insertFileSel(6,120,true,
                            info.getSavesDirectory(), false);
   }
   else
   {
      fileSelector = fileWindow->getObjectsList()->insertFileSel(6,18,false,
                            info.getSavesDirectory(), false);
   }

   /* Define the filter */
   fileSelector->setFilter(".sav");

   /* Open the window */
   fileWindow->setAttributes(false,true,false,false);
   fileWindow->setExternPointer(&fileWindow);
   gui->openWindow(fileWindow);

}

/***********************************************************************
 *                           changeInfo                                *
 ***********************************************************************/
void SaveWindow::changeInfo(int mouseX, int mouseY)
{
   saveFile* sav;
   Farso::Warning warn;

   /* Free any previous */
   if(windowIsLoad)
   {
      if(fileImage->get() != NULL)
      {
         SDL_FreeSurface(fileImage->get());
         fileImage->set(NULL);
      }
      fileTitle->setText("");

      /* Reload a new, if defined */
      if(!fileSelector->getFileName().empty())
      {
         sav = new saveFile;
         if(sav->loadHeader(fileSelector->getFileName()))
         {
            fileImage->set(IMG_Load(sav->getImageFile().c_str()));
            fileTitle->setText(sav->getTitle());
         }
         else
         {
            warn.show(gettext("Error"), gettext("Invalid File!"), gui);
         }
         delete(sav);
      }

      fileWindow->draw(mouseX, mouseY);
   }
}

/***********************************************************************
 *                                 run                                 *
 ***********************************************************************/
int SaveWindow::run(bool load, GLuint tituloId)
{
   int state = -1;
   int time=0, lastTime=0;
   int eventInfo;
   options option;
   DNT::Mouse mouse;
   Farso::Farso farso;

   /* Define type */
   windowIsLoad = load;

   /* Create the GUI */
   gui = new Farso::GuiInterface("");

   /* Open the window */
   open();

   glDisable(GL_LIGHTING);
   glDisable(GL_FOG);
   while(state == -1)
   {
      time = SDL_GetTicks();
      if(time - lastTime >= UPDATE_RATE)
      {
         lastTime = time;
         /* Clear things */
         glClearColor(0,0,0,1);
         glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

         /* Get Events */
         farso.updateInputState();
         gui->manipulateEvents(eventInfo);

         /* Render Things */
         glPushMatrix();
            draw2DMode();
            Farso::textureToScreen(tituloId,0,0,
                  Farso::SCREEN_X-1,Farso::SCREEN_Y-1,800,600);
            gui->draw();
            glPushMatrix();
               mouse.draw();
            glPopMatrix();
            draw3DMode(option.getFarViewFactor()*OUTDOOR_FARVIEW);
         glPopMatrix();
         
         glFlush();
         farso.swapBuffers();

         /* Get Events  */
         switch(eventInfo)
         {
            case Farso::EVENT_FILE_SEL_CHANGED:
               changeInfo(mouse.getX(), mouse.getY());
            break;
            case Farso::EVENT_FILE_SEL_CANCEL:
               state = DNT_SAVE_WINDOW_CANCEL;
            break;
            case Farso::EVENT_FILE_SEL_ACCEPT:
               curFileName = fileSelector->getFileName();
               state = DNT_SAVE_WINDOW_CONFIRM; 
            break;
         }
      }
      else if((UPDATE_RATE-1) - (time - lastTime) > 0 )
      {
         SDL_Delay((UPDATE_RATE-1) - (time - lastTime) );
      }
   }

   /* Delete the gui and textures */
   delete gui;

   glEnable(GL_LIGHTING);
   glEnable(GL_FOG);

   return state;

}

