/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_minimap_window_h
#define _dnt_minimap_window_h

#include "../gui/farso.h"
#include "../map/map.h"
#include <string>
#include <list>

/*! A label in the minimap */
class miniMapLabel
{
   public:
      std::string labelText;   /**< The caption text for label */
      std::string labelName;   /**< The label name */

      vec2_t position;         /**< Label map position */
};


/*! The minimap window */
class MiniMapWindow
{
   public:
      /*! Constructor */
      MiniMapWindow();
      /*! Destructor */
      ~MiniMapWindow();

      /*! Open the minimap window (if not already opened)
       * \param posX -> character X position
       * \param posZ -> character Z position 
       * \param openedMap -> current opened map */
      void open(float posX, float posZ, Map* openedMap);

      /*! Close the minimap window (if opened) */
      void close();

      /*! Update the active character map position
       * \param posX -> character X position
       * \param posZ -> character Z position */
      void updateCharacterPosition(float posX, float posZ);

      /*! Verify if the minimap window is opened
       * \return -> true if is opened, false otherwise */
      bool isOpened();

      /*! Reopen the Window (if is opened) 
       * \param openedMap -> current opened map */
      void reOpen(Map* openedMap);

   protected:
      /*! Set map connections labels on minimap */
      void setConnections();
      /*! Get label on the list */
      miniMapLabel* getLabel(float x, float y, std::string name);
      /*! Clear label list */
      void clearLabels();

      static std::list<miniMapLabel*>labels; /**< Labels on map */
      static int width;             /**< Current picture width */
      static int height;            /**< Current picture height */
      static SDL_Surface* connectionImage; /**< Connection Image */
      static Farso::Picture* fig;          /**< The current minimap picture */
      static Farso::Window* mapWindow;     /**< MiniMap Window */
      static Farso::Button* charPosition;  /**< Character MiniMap Button */
      static Map* curMap;           /**< Current Opened Map */
      static Farso::Farso farso;  /**< Current Farso */
};

#endif

