/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_racewindow_h
#define _dnt_racewindow_h

#include "../gui/farso.h"
#include "../classes/race.h"

#define RACEW_CANCEL  0
#define RACEW_CONFIRM 1
#define RACEW_OTHER   2

/*! Races Window */
class RaceWindow
{
   public:
      /*! Constructor
       * \param retRace -> pointer where will be the choosed race */
      RaceWindow(race** retRace);

      /*! Destructor */
      ~RaceWindow();

      /*! Treat Events on Window. 
       * \param inter -> pointer to GUI interface
       * \param object -> last GUI object
       * \param eventInfo -> last GUI Event
       * \return 0 on close, 1 otherwise */
      int treat(Farso::GuiObject* object, int eventInfo);

      Farso::Window*      intWindow;      /**< Pointer to the internal window */

   private:
      int         curRace;         /**< Position of current race on ordered */
      int         totalRaces;      /**< total Races */
      race**      choosedRace;     /**< Pointer to the choosed pointer race */
      race**      racesOrder;      /**< Vector with races ordered by name */

      Farso::Farso farso;
      Farso::Button* buttonConfirm;
      Farso::Button* buttonCancel;

      Farso::Button* buttonPrevious;
      Farso::Button* buttonNext;

      Farso::TextBox* textName;
      Farso::RolBar* textDesc;
      Farso::RolBar* textCharac;

      Farso::Picture* raceImage;

      void setCharacteristics();

      void setDescription();

};

#endif
