/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_classwindow_h
#define _dnt_classwindow_h

#include "../gui/farso.h"
#include "../classes/classes.h"
#include "../classes/feats.h"

#define CLASSW_CANCEL  0
#define CLASSW_CONFIRM 1
#define CLASSW_OTHER   2

/*! Races Window */
class ClassWindow
{
   public:
      /*! Constructor 
       * \param ftl -> pointer to the featsList
       * \param retClass -> pointer to the choosed class */
      ClassWindow(classe** retClass);

      /*! Destructor */
      ~ClassWindow();

      /*! Treat Events on Window. 
       * \param object -> last GUI object
       * \param eventInfo -> last GUI Event
       * \return 0 on close, 1 otherwise */
      int treat(Farso::GuiObject* object, int eventInfo); 

      Farso::Window*      intWindow;  /**< Pointer to the internal window */

   private:
      int           curClass;         /**< Pointer to current Class */
      classe**      classesOrder;     /**< Alphabetical ordered classes */
      classe**      choosedClass;     /**< Pointer to choosed pointer class */
      int           totalClasses;     /**< Total Avaible classes */

      Farso::Farso farso;
      Farso::Button* buttonConfirm;
      Farso::Button* buttonCancel;

      Farso::Button* buttonPrevious;
      Farso::Button* buttonNext;

      Farso::TextBox* textName;
      Farso::RolBar* textDesc;
      Farso::RolBar* textCharac;

      Farso::Picture* classImage;

      /*! Draw the characteritics */
      void setCharacteristics();
      /*! Draw the descriptions */
      void setDescription();

};

#endif
