/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef dnt_journal_window_h
#define dnt_journal_window_h

#include "../gui/farso.h"
#include "../etc/btree.h"
#include "../etc/list.h"

/*! The journalDesc keeps information about a single mission */
class journalDesc: public DntListElement
{
   public:
      std::string text;  /**< Description Text */
      int completed;     /**< If completed or not */
};

/*! The journalArea keeps information about
 * all missions on an specific area */
class journalArea : public BTreeCell, public DntList
{
   public:
      /*! Constructor */
      journalArea();

      /*! Destructor */
      ~journalArea();
 
      /*! Compare with another cell
       * \param cell -> pointer to cell to compare this one to
       * \return == 0 if equal, < 0  if lesser or > 0 if greater */
      int compare(BTreeCell* cell);

      /*! Merge the current BTreeCell with another
       * \param cell -> cell to merge the current one with */
      void merge(BTreeCell* cell);

      /*! Insert a mission at the area
       * \param desc -> mission description
       * \param comp -> >0 if completed, 0 if current, <0 if failed */
      void insertMission(std::string desc, int comp);

      /*! Get the area name
       * \return string with area's name */
      std::string getName();

      /*! Set the area name
       * \param n -> new area's name */
      void setName(std::string n);

   protected:

      std::string name;  /**< Area Name */
};

/*! The journalAreas keeps all journalArea informations */
class journalAreas : public BTree
{
   public:
      /*! Constructor */
      journalAreas();
      /*! Destructor */
      ~journalAreas();

      /*! Insert a journalArea at the BTree
       * \param title -> journalArea text 
       * \return -> pointer to the inserted journal area */
      journalArea* insert(std::string title);

      /*! Insert a journalArea at the BTree
       * \param title -> journalArea text 
       * \return -> pointer to the inserted journal area */
      journalArea* search(std::string title);

      /*! Get the journalArea at the num position
       * \param num -> position to get
       * \return -> pointer to the area got */
      journalArea* get(int num);

      /*! Get the journalArea at the num position
       * \param num -> position to get
       * \param curRoot -> current node used as root
       * \param cur -> current node position 
       * \return -> pointer to the area got */
      journalArea* get(int num, journalArea* curRoot, int& cur);

      /*! Dupplicate a cell (aka: alloc a new one with same values)
       * \param cell -> pointer to the cell to dupplicate */
      BTreeCell* dupplicateCell(BTreeCell* cell);

};

/*! The Journal Window definition.
 * This window shows current, finished and aborted missions
 * sorted by game area.
 * \todo -> TODO: the AREA idea implementation. Currently it's only a 
 *                separator at missions definitions. */
class JournalWindow
{
   public:
      /*! Constructor */
      JournalWindow();
      /*! Destructor */
      ~JournalWindow();

      /*! Open, if not already opened, the journal window */
      void open();

      /*! Verify if the window is opened
       * \return true if the JournalWindow is opened */
      bool isOpen();
      /*! Close, if opened, the journal window */
      void close();

   protected:
      /*! Create the Journal missions lists per area */
      void createLists();

      /*! Show current Area missions at the window */
      void showArea();

      journalAreas* areas;       /**< The missions per areas */
      int curArea;               /**< Current area displayed */

      Farso::Farso farso;              /**< The farso used */
      Farso::Window* internalWindow;    /**< The window pointer */
      Farso::Button* nextButton;        /**< The Next Area Button */
      Farso::Button* previousButton;    /**< The Previous Area Button */
      Farso::TextBox* areaText;         /**< The Area text */
      Farso::RolBar* missionsText;      /**< The missons text */
};

#endif

