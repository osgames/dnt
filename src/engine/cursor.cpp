/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "cursor.h"

#include "util.h"
#include "../etc/dirs.h"
#include "../gui/farsoopts.h"

#include <iostream>
using namespace std;
using namespace DNT;

/*****************************************************************
 *                          loadAll                              *
 *****************************************************************/
void Mouse::loadAll()
{
   dirs dir;
   int i;
   
   /* Load Cursors */
   for(i=0; i < CURSOR_TOTAL; i++)
   {
      MouseCursor::set(dir.getRealFile(cursorFile[i]), 
            cursorHotSpot[i][0], cursorHotSpot[i][1]);
   }
   loadedAll = true;

   /* set the current */
   set(CURSOR_WALK);
}

/*****************************************************************
 *                               set                             *
 *****************************************************************/
void Mouse::set(int nCursor)
{
   /* Load cursors, if needed. */
   if(!loadedAll)
   {
      loadAll();
   }

   if((nCursor >= 0) && (nCursor < CURSOR_TOTAL))
   {
      dirs dir;
      currentCursor = nCursor;
      MouseCursor::set(dir.getRealFile(cursorFile[nCursor]), 
            cursorHotSpot[nCursor][0], cursorHotSpot[nCursor][1]);
   }
   else if(nCursor < 0)
   {
      hide();
   }
}

/*****************************************************************
 *                              set                              *
 *****************************************************************/
void Mouse::set(SDL_Surface* img)
{
   MouseCursor::set(img);
   currentCursor = CURSOR_USER_IMAGE;
}

/*****************************************************************
 *                               get                             *
 *****************************************************************/
int Mouse::get()
{
   return currentCursor;
}

/*****************************************************************
 *                         Static Members                        *
 *****************************************************************/
int Mouse::currentCursor = -1;
bool Mouse::loadedAll = false;
const std::string Mouse::cursorFile[] =
{
   "cursors/Walk.png", "cursors/Attack.png", "cursors/Defend.png", 
   "cursors/MapTravel.png", "cursors/talk.png", "cursors/Get.png",
   "cursors/Inventory.png", "cursors/Door.png", "cursors/forbidden.png",
   "cursors/walk_cont.png", "cursors/use.png"
};
const int Mouse::cursorHotSpot[][2] =
{ 
   { 0, 0}, { 1, 0}, { 0, 0},
   {15,15}, {15,15}, { 5, 2},
   {15,15}, { 5, 0}, {15,15},
   { 0, 0}, {15,15} 
};

