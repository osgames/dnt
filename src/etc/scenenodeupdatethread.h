/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_model_update_thread_h
#define _dnt_model_update_thread_h

#include <SDL2/SDL.h>

#include "scenenode.h"

namespace DNT
{

/*! This class is the representation of data used by nodes update threads.
 * Those threads are used to parallel update SceneNodes animations. */
class SceneNodeUpdateThread
{
   public:
      /*! Constructor: create the thread */
      SceneNodeUpdateThread();
      /*! Destructor: delete the thread */
      ~SceneNodeUpdateThread();

      /*! Lock thread mutex */
      void lock();
      /*! Unlock thread mutex */
      void unlock();

      /*! Set scene node to update or none if thread done. */
      void setSceneNode(SceneNode* sceneNode);
      /*! \return current scene node to update, if any. */
      SceneNode* getSceneNode();

      /*! Wait receive the wakt condition signal.
       * \note must be inside lock() unlock() calls. */
      void waitWakeSignal();

      /*! \return if thread is busy with a sceneNode or not. */
      bool isBusy();

      /*! \return true if is to end the thread execution */
      bool shouldEnd();
   
   private: 

      SDL_Thread* thread; /**< The thread itself */
      SDL_mutex* mutex; /**< Mutex to control condition */
      SDL_cond* cond; /**< Condition */

      SceneNode* sceneNode; /**< Current scene node to use */

      bool mustEnd; /**< if must end the main function execution */
      
};

}

#endif

