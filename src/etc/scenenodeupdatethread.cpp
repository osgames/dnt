/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "scenenodeupdatethread.h"

using namespace DNT;

/*! Main function of the SceneNodeUpdateThread */
int initSceneNodeUpdateThread(void* thread);

/************************************************************************
 *                          Main Function                               *
 ************************************************************************/
int initSceneNodeUpdateThread(void* thread)
{
   SceneNodeUpdateThread* t = (SceneNodeUpdateThread*) thread;
   SceneNode* sceneNode = NULL;

   while(!t->shouldEnd())
   {
      t->lock();
      sceneNode = t->getSceneNode();
      if(sceneNode == NULL)
      {
         /* No scene node defined, must sleep until have one. */
         t->waitWakeSignal();
         sceneNode = t->getSceneNode();
      }
      t->unlock();

      /* Should have a scene node now, or MAX_TIME
       * was reached, must check if have the node before go further.  */
      if(sceneNode != NULL)
      {
         sceneNode->update();

         /* Done with scene node update, must be vacant. */
         t->setSceneNode(NULL);
      }
   }

   return 0;
}

/************************************************************************
 *                          constructor                                 *
 ************************************************************************/
SceneNodeUpdateThread::SceneNodeUpdateThread()
{
   mustEnd = false;
   mutex = SDL_CreateMutex();
   cond = SDL_CreateCond();
   sceneNode = NULL;

   /* Start the thread */
   thread  = SDL_CreateThread(&initSceneNodeUpdateThread, NULL, this);
}

/************************************************************************
 *                           destructor                                 *
 ************************************************************************/
SceneNodeUpdateThread::~SceneNodeUpdateThread()
{
   lock();
   mustEnd = true;
   if(getSceneNode() == NULL)
   {
      /* Must awake the thread, as it should be sleeping. */
      SDL_CondSignal(cond);
   }
   unlock();
   SDL_WaitThread(thread, NULL);
   SDL_DestroyCond(cond);
   SDL_DestroyMutex(mutex);
}

/************************************************************************
 *                           shouldEnd                                  *
 ************************************************************************/
bool SceneNodeUpdateThread::shouldEnd()
{
   return mustEnd;
}

/************************************************************************
 *                        waitWakeSignal                                *
 ************************************************************************/
void SceneNodeUpdateThread::waitWakeSignal()
{
   SDL_CondWait(cond, mutex);
}

/************************************************************************
 *                           getSceneNode                               *
 ************************************************************************/
void SceneNodeUpdateThread::setSceneNode(SceneNode* sceneNode)
{
   lock();
   bool wasNull = (this->sceneNode == NULL);
   this->sceneNode = sceneNode;
   if( (wasNull) && (sceneNode != NULL) )
   {
      /* Defined a new scene node to update, must tell the thread. */
      SDL_CondSignal(cond);
   }
   unlock();
}

/************************************************************************
 *                           getSceneNode                               *
 ************************************************************************/
SceneNode* SceneNodeUpdateThread::getSceneNode()
{
   return sceneNode;
}

/************************************************************************
 *                              isBusy                                  *
 ************************************************************************/
bool SceneNodeUpdateThread::isBusy()
{
   return sceneNode != NULL;
}

/************************************************************************
 *                              lock                                    *
 ************************************************************************/
void SceneNodeUpdateThread::lock()
{
   SDL_LockMutex(mutex);
}

/************************************************************************
 *                              unlock                                  *
 ************************************************************************/
void SceneNodeUpdateThread::unlock()
{
   SDL_UnlockMutex(mutex);
}

