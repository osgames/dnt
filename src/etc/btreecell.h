/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_btreecell_h
#define _dnt_btreecell_h

/*! A binary tree cell definition  */
class BTreeCell
{
   public:
      /*! Constructor */
      BTreeCell();
      /*! Desctuctor */
      virtual ~BTreeCell();

      /*! Gets the left son 
       * \return the cell's left son */
      BTreeCell* getLeft();
      /*! Sets the left son 
       * \param cell -> reference to the son to be */
      void setLeft(BTreeCell* cell);
      /*! Gets the right son 
       * \return the cell's right son */
      BTreeCell* getRight();
      /*! Sets the right son 
       * \param right -> reference to the son to be */
      void setRight(BTreeCell* right);

      /*! Compare with another cell
       * \param cell -> pointer to cell to compare this one to
       * \return == 0 if equal, < 0  if lesser or > 0 if greater */
      virtual int compare(BTreeCell* cell) = 0;

      /*! Merge the current BTreeCell with another
       * \param cell -> cell to merge the current one with */
      virtual void merge(BTreeCell* cell) = 0;

   private:
      BTreeCell* left;   /**< The cell's left son */
      BTreeCell* right;  /**< The cell's right son */ 
};

#endif

