/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _dnt_scene_h
#define _dnt_scene_h

#include "animodel.h"
#include "scenenode.h"
#include "scenenodeupdatethread.h"
#include "list.h"

#define ANIMATED_LIST_NAME  "animatedListName"
#define NUM_UPDATE_THREADS  3

/*! The render position is just a SceneNode for list */
class RenderPosition: public DntListElement
{
   public:
      /*! Destructor */
      ~RenderPosition();

      SceneNode* node; /**< The related node*/
};

/*! A list of nodes */
class NodesList: public DntListElement, public DntList
{
   public:
      /*! Constructor for a list of animated nodes
       * \note: list name will be ANIMATED_LIST_NAME */
      NodesList();
      /*! Constructor for a list of inanimated nodes of specific model
       * \note: listName will be modelFileName */
      NodesList(std::string modelFileName);
      /*! Destructor */
      ~NodesList();

      /*! Render all models on list */
      void render(bool reflexion, bool shadow, GLfloat* shadowMatrix, 
            float alpha);

      /*! If is an animated models list, update all animations and poses.
       * If static, should never be called. */
      void update();

      /*! Get scene node inner a bounding box
       * \param scBox -> bounding box to check SceneNode in
       * \return pointer to the SceneNode or NULL */
      SceneNode* getSceneNode(boundingBox& scBox);

      /*! Delete scene node from list */
      void deleteSceneNode(SceneNode* node);

      /*! Get the nodes list name */
      std::string getName(){return(name);};
      /*! Get the nodes aniModel */
      aniModel* getModel(){return(model);};

      /*! \return If is a list of static models (not animated).
       * For those, a single model object is loaded and instanced
       * by all its positions. */
      bool isStatic();

   protected:

      /*! Get a not busy update thread to use */
      DNT::SceneNodeUpdateThread* getVacantThread();

      /*! Wait all busy update threads to end */
      void waitBusyThreads();
    
      aniModel* model;  /**< Model loaded or NULL */
      std::string name;      /**< Name of the list */

      DNT::SceneNodeUpdateThread* updateThread[NUM_UPDATE_THREADS];
};

/*! A list of nodes lists   */
class ListNodesList: public DntList
{
   public:
      /*! Constructor */
      ListNodesList();
      /*! Destructor */
      ~ListNodesList();

      /*! Find a list of name
       * \param name -> name of the list 
       * \return list pointer or NULL */
      NodesList* find(std::string name);
      /*! Find a list of model
       * \param model -> pointer to the model to find list of
       * \return list pointer or NULL */
      NodesList* find(aniModel* model);

      /*! Render all models on list */
      void render(bool reflexion, bool shadow, GLfloat* shadowMatrix, 
            float alpha);

      /*! Get scene node inner a bounding box
       * \param scBox -> bounding box to check SceneNode in
       * \return pointer to the SceneNode or NULL */
      SceneNode* getSceneNode(boundingBox& scBox);

};

/*! A scene is the controller of all 3d models on the game */
class Scene
{
   public:
      /*! Constructor */
      Scene();
      /*! Destructor */
      ~Scene();

      /*! Init the scene to use */
      void init();
      /*! Finish the use of scene */
      void finish();
      /*! Remove All Unused Models */
      void removeUnusedModels();

      /*! Create an scene node  */
      SceneNode* createSceneNode(bool staticModel, std::string modelFileName, 
            GLfloat x, GLfloat y, GLfloat z,
            GLfloat aX, GLfloat aY, GLfloat aZ);

      /*! Get first SceneNode in the bounding box
       * \param scBox -> bounding box to get scene nodes in
       * \return -> pointer to SceneNode inner boundingBox or NULL if none.*/
      SceneNode* getSceneNode(boundingBox& scBox);

      /*! Delete an scene node from scene */
      void deleteSceneNode(SceneNode* node);

      /*! Render all models on scene, doing frustum culling */
      void render(bool update, bool reflexion, bool shadow, 
            GLfloat* shadowMatrix, float alpha);

   protected:
      static ListNodesList* nLists; /**< List of nodes list */
      static NodesList* animatedList; /**< List of animated models */ 
};

#endif

