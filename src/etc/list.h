/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_list_h
#define _dnt_list_h

/*! The DntList types */
enum
{
   DNT_LIST_TYPE_ADD_AT_BEGIN=0,
   DNT_LIST_TYPE_ADD_AT_END
};

/*! The list element is the base class to define an element of a list */
class DntListElement
{
   public:
      /*! Constructor */
      DntListElement();
      /*! Destructor */
      virtual ~DntListElement();

      /*! Get the next element
       * \return pointer to the next element */
      DntListElement* getNext();
      /*! Get the previous element
       * \return pointer to the previous element */
      DntListElement* getPrevious();

      /*! Set the next element
       * \param obj -> pointer to the next element on the list */
      void setNext(DntListElement* obj);
      /*! Set the previous element
       * \param obj -> pointer to the previous element on the list */
      void setPrevious(DntListElement* obj);

   protected:

      DntListElement* next;           /**< Next on list */
      DntListElement* previous;       /**< Previous on list */

};

/*! The basic chain list implementation */
class DntList
{
   public:
      /*! Constructor 
       * \param t -> list type 
       * \param willDeleteOnDestructor if list will be empty and all
       *        its elements deleted on the destructor. */
      DntList(int t=DNT_LIST_TYPE_ADD_AT_BEGIN, 
              bool willDeleteOnDestructor=true);
      /*! Destructor
       * \note the destructor will delete all elements still on list,
       *       if defined at the constructor */
      virtual ~DntList();

      /*! Clear all list elements */
      void clearList();

      /*! Insert an element on the list, at the default position
       * \param obj -> object to insert
       * \return true if ok */
      bool insert(DntListElement* obj);

      /*! Insert element at the list's begin 
       * \param obj -> object to insert
       * \return true if ok */
      bool insertAtBegin(DntListElement* obj);

      /*! Insert element at list's end 
       * \param obj -> object to insert
       * \return true if ok */
      bool insertAtEnd(DntListElement* obj);

      /*! Remove an element from the list
       * \param obj -> element to remove
       * \note This function will delete the element when returning true.
       * \return true if ok */
      bool remove(DntListElement* obj);

      /*! Only remove the element from the list, without deleting it
       * \param obj -> obj to remove
       * \return true if ok */
      bool removeWithoutDelete(DntListElement* obj);

       /*! Get total elements on the list
       * \return total elements on the list */
      int getTotal();

      /*! Get the fisrt element
       * \return pointer to the first element */
      DntListElement* getFirst();

      /*! Get element from list */
      DntListElement* get(int i);

   protected:
      
      DntListElement* first;          /**< Pointer to the first list element */
      int total;                      /**< Total elements on the list */

   private:
      int listType;                   /**< list type (FIFO, LIFO) */
      bool deleteOnDestructor; /**< If should delete on destructor or not. */
};

#endif

