/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "scene.h"
using namespace std;

/***********************************************************************
 *                             constructor                             *
 ***********************************************************************/
NodesList::NodesList()
{
   name = ANIMATED_LIST_NAME;
   model = NULL;
   for(int i = 0; i < NUM_UPDATE_THREADS; i++)
   {
      updateThread[i] = new DNT::SceneNodeUpdateThread();
   }
}

/***********************************************************************
 *                             constructor                             *
 ***********************************************************************/
NodesList::NodesList(string modelFileName)
{
   name = modelFileName;

   /* Load the model, based on its type */
   if(modelFileName.find(".md5") != string::npos)
   {
      model = new md5Model();
   }
   else
   {
      model = new cal3DModel();
   }

   model->load(modelFileName);
   model->calculateCrudeBoundingBox();

   for(int i = 0; i < NUM_UPDATE_THREADS; i++)
   {
      updateThread[i] = NULL;
   }

}

/***********************************************************************
 *                              destructor                             *
 ***********************************************************************/
NodesList::~NodesList()
{
   clearList();
   /* Delete the static model */
   if(model)
   {
      switch(model->getType())
      {
         case aniModel::TYPE_CAL3D:
         {
            cal3DModel* c = (cal3DModel*)model;
            delete(c);
         }
         break;
         case aniModel::TYPE_MD5:
         {
            md5Model* m = (md5Model*)model;
            delete(m);
         }
         break;
         default:
         {
            delete(model);
         }
         break;
      }
   }
   else
   {
      /* Delete the update threads */
      for(int i = 0; i < NUM_UPDATE_THREADS; i++)
      {
         delete updateThread[i];
      }
   }
}

/***********************************************************************
 *                            getSceneNode                             *
 ***********************************************************************/
SceneNode* NodesList::getSceneNode(boundingBox& scBox)
{
   int i;
   RenderPosition* rp = (RenderPosition*)getFirst();

   /* Verify at animated list */
   for(i=0; i < total; i++)
   {
      if(rp->node->getBoundingBox().intercepts(scBox))
      {
         /* Interceptd... must return */
         return(rp->node);
      }
      rp = (RenderPosition*)rp->getNext();
   }

   return(NULL);
}

/***********************************************************************
 *                          deleteSceneNode                            *
 ***********************************************************************/
void NodesList::deleteSceneNode(SceneNode* node)
{
   /* search for the node */
   int i;
   RenderPosition* sc = (RenderPosition*)first;

   /* Find list */
   for(i=0; i < total; i++)
   {
      if(sc->node == node)
      {
         remove(sc);
         return;
      }
      sc = (RenderPosition*)sc->getNext();
   }

   /* node found on list! */
   cerr << "Couldn't find SceneNode '" << node << "' on list!" << endl;
   delete(node);
}

/***********************************************************************
 *                             isStatic                                *
 ***********************************************************************/
bool NodesList::isStatic()
{
   return model != NULL;
}

/***********************************************************************
 *                         getVacantThread                             *
 ***********************************************************************/
DNT::SceneNodeUpdateThread* NodesList::getVacantThread()
{
   int i = 0;

   while(updateThread[i]->isBusy())
   {
      i = (i + 1) % NUM_UPDATE_THREADS;
   }

   return updateThread[i];
}

/***********************************************************************
 *                         waitBusyThreads                             *
 ***********************************************************************/
void NodesList::waitBusyThreads()
{
   for(int i=0; i < NUM_UPDATE_THREADS; i++)
   {
      while(updateThread[i]->isBusy());
   }
}

/***********************************************************************
 *                                update                               *
 ***********************************************************************/
void NodesList::update()
{
   RenderPosition* pos = (RenderPosition*)first; 

   /* Let's render all */
   for(int i = 0; i < total; i++)
   {
      //TODO: optimization: only clear if camera moved. 
      pos->node->clearVisibleCache();
      getVacantThread()->setSceneNode(pos->node);
      pos = (RenderPosition*)pos->getNext();
   }
   waitBusyThreads();
}

/***********************************************************************
 *                                render                               *
 ***********************************************************************/
void NodesList::render(bool reflexion, bool shadow, GLfloat* shadowMatrix, 
      float alpha)
{
   bool modelLoaded = false;
   int i;
   RenderPosition* pos = (RenderPosition*)first; 

   /* Let's render all */
   for(i=0; i < total; i++)
   {
      if(isStatic())
      {
         /* Make sure the visible cache is clean, as the camera should
          * changed. */
         //TODO: only clean if the camera changed!
         pos->node->clearVisibleCache();
      }
      else 
      {
         /* Trick for Animated list, set to false to force load of next model */
         modelLoaded = false;
      }
      pos->node->render(reflexion, shadow, shadowMatrix, alpha, modelLoaded);
      pos = (RenderPosition*)pos->getNext();
   }

   if( (modelLoaded)  && (model) )
   {
      /* If static and loaded, must remove from graphic memory */
      model->removeFromGraphicMemory();
   }      
}

/***********************************************************************
 *                             destructor                              *
 ***********************************************************************/
RenderPosition::~RenderPosition()
{
   delete node;
}

/***********************************************************************
 *                             constructor                             *
 ***********************************************************************/
ListNodesList::ListNodesList()
{
}

/***********************************************************************
 *                              destructor                             *
 ***********************************************************************/
ListNodesList::~ListNodesList()
{
   clearList();
}

/***********************************************************************
 *                                 find                                *
 ***********************************************************************/
NodesList* ListNodesList::find(string name)
{
   int i;
   NodesList* l = (NodesList*)first;

   /* Find list */
   for(i=0; i < total; i++)
   {
      if(l->getName() == name)
      {
         return(l);
      }
      l = (NodesList*)l->getNext();
   }

   return(NULL);
}

/***********************************************************************
 *                                 find                                *
 ***********************************************************************/
NodesList* ListNodesList::find(aniModel* model)
{
   int i;
   NodesList* l = (NodesList*)first;

   /* Find list */
   for(i=0; i < total; i++)
   {
      if(l->getModel() == model)
      {
         return(l);
      }
      l = (NodesList*)l->getNext();
   }

   return(NULL);
}

/***********************************************************************
 *                               render                                *
 ***********************************************************************/
void ListNodesList::render(bool reflexion, bool shadow, 
      GLfloat* shadowMatrix, float alpha)
{
   int i;
   NodesList* l = (NodesList*)first;

   /* Find list */
   for(i=0; i < total; i++)
   {
      l->render(reflexion, shadow, shadowMatrix, alpha);
      l = (NodesList*)l->getNext();
   }
}

/***********************************************************************
 *                            getSceneNode                             *
 ***********************************************************************/
SceneNode* ListNodesList::getSceneNode(boundingBox& scBox)
{
   int i;
   NodesList* l = (NodesList*)getFirst();
   SceneNode* scNode=NULL;

   /* Verify at animated list */
   for(i=0; i < total; i++)
   {
      /* Verify interception on this list */
      scNode = l->getSceneNode(scBox);
      if(scNode)
      {
         return(scNode);
      }

      /* None... must verify next */
      l = (NodesList*)l->getNext();
   }

   return(NULL);
}


/***********************************************************************
 *                             constructor                             *
 ***********************************************************************/
Scene::Scene()
{
}

/***********************************************************************
 *                              destructor                             *
 ***********************************************************************/
Scene::~Scene()
{
}

/***********************************************************************
 *                                  init                               *
 ***********************************************************************/
void Scene::init()
{
   nLists = new ListNodesList();
   animatedList = new NodesList();
}

/***********************************************************************
 *                                  finish                             *
 ***********************************************************************/
void Scene::finish()
{
   delete nLists;
   delete animatedList;
}

/***********************************************************************
 *                        removeUnusedModels                           *
 ***********************************************************************/
void Scene::removeUnusedModels()
{
   int i;
   int total = nLists->getTotal();
   NodesList* l=NULL;

   /* Search for lists without SceneNodes */
   l = (NodesList*)nLists->getFirst();
   for(i=0; i < total; i++)
   {
      if(l->getTotal() == 0)
      {
         l = (NodesList*)l->getNext();
         nLists->remove(l->getPrevious());
      }
      else
      {
         l = (NodesList*)l->getNext();
      }
   }
}

/***********************************************************************
 *                               render                                *
 ***********************************************************************/
void Scene::render(bool update, bool reflexion, bool shadow, 
      GLfloat* shadowMatrix, float alpha)
{
   nLists->render(reflexion, shadow, shadowMatrix, alpha);
   if(update)
   {
      animatedList->update();
   }
   animatedList->render(reflexion, shadow, shadowMatrix, alpha);
}

/***********************************************************************
 *                          createSceneNode                            *
 ***********************************************************************/
SceneNode* Scene::createSceneNode(bool staticModel, string modelFileName,
      GLfloat x, GLfloat y, GLfloat z, GLfloat aX, GLfloat aY, GLfloat aZ)
{
   RenderPosition* rp = new RenderPosition;
   rp->node = NULL;

   if(staticModel)
   {
      /* Verify if a list exists for this model */
      NodesList* l = nLists->find(modelFileName);
      if(!l)
      {
         /* No list for it, must create a new one */
         l = new NodesList(modelFileName);
         nLists->insert(l);
      }
      /* Create and insert the new SceneNode */
      rp->node = new SceneNode(l->getModel(), x,y,z, aX,aY,aZ);
      l->insert(rp);
   }
   else
   {
      /* Create the scene node & render position */
      rp->node = new SceneNode(modelFileName, x,y,z, aX,aY,aZ);

      /* insert it at the animation list */
      animatedList->insert(rp);
   }

   return(rp->node);
}

/***********************************************************************
 *                            getSceneNode                             *
 ***********************************************************************/
SceneNode* Scene::getSceneNode(boundingBox& scBox)
{
   SceneNode* scNode = NULL;

   /* Verify at animated list */
   scNode = animatedList->getSceneNode(scBox);

   /* Verify at static nodes list */
   if(!scNode)
   {
      scNode = nLists->getSceneNode(scBox);
   }

   return(scNode);
}

/***********************************************************************
 *                          deleteSceneNode                            *
 ***********************************************************************/
void Scene::deleteSceneNode(SceneNode* node)
{
   if(node->isAnimated())
   {
      animatedList->deleteSceneNode(node);
   }
   else
   {
      NodesList* l = nLists->find(node->getModel());
      if(l)
      {
         l->deleteSceneNode(node);
      }
      else
      {
         cerr << "Couldn't find list for node! " << node->getModel() << endl;
         delete(node);
      }
   }
}


ListNodesList* Scene::nLists=NULL;
NodesList* Scene::animatedList=NULL;

