/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "list.h"
#include <stdlib.h>


///////////////////////////////////////////////////////////////////////////
//                                                                       //
//                            DntListElement                             //
//                                                                       //
///////////////////////////////////////////////////////////////////////////

/***********************************************************************
 *                            Constructor                              *
 ***********************************************************************/
DntListElement::DntListElement()
{
   next = NULL;
   previous = NULL;
}

/***********************************************************************
 *                             Destructor                              *
 ***********************************************************************/
DntListElement::~DntListElement()
{
}

/***********************************************************************
 *                              getNext                                *
 ***********************************************************************/
DntListElement* DntListElement::getNext()
{
   return next;
}

/***********************************************************************
 *                            getPrevious                              *
 ***********************************************************************/
DntListElement* DntListElement::getPrevious()
{
   return previous;
}

/***********************************************************************
 *                                setNext                              *
 ***********************************************************************/
void DntListElement::setNext(DntListElement* obj)
{
   next = obj;
}

/***********************************************************************
 *                              setPrevious                            *
 ***********************************************************************/
void DntListElement::setPrevious(DntListElement* obj)
{
   previous = obj;
}

///////////////////////////////////////////////////////////////////////////
//                                                                       //
//                               DntList                                 //
//                                                                       //
///////////////////////////////////////////////////////////////////////////

/***********************************************************************
 *                            Constructor                              *
 ***********************************************************************/
DntList::DntList(int t, bool willDeleteOnDestructor)
{
   first = NULL;
   total = 0;
   listType = t;
   deleteOnDestructor = willDeleteOnDestructor;
}

/***********************************************************************
 *                             Destructor                              *
 ***********************************************************************/
DntList::~DntList()
{
   if(deleteOnDestructor) 
   {
      clearList();
   }
}

/***********************************************************************
 *                             clearList                               *
 ***********************************************************************/
void DntList::clearList()
{
   int i;
   DntListElement* e = first;
   DntListElement* paux;

   /* Delete all elements */
   for(i=0; i<total; i++)
   {
      paux = e;
      e = e->getNext();
      delete paux;
   }

   first = NULL;
   total = 0;
}

/***********************************************************************
 *                               insert                                *
 ***********************************************************************/
bool DntList::insert(DntListElement* obj)
{
   if(obj == NULL)
   {
      return false;
   }

   if(listType == DNT_LIST_TYPE_ADD_AT_BEGIN)
   {
      return insertAtBegin(obj);
   }
   /* Else */
   return insertAtEnd(obj);
}

/***********************************************************************
 *                         insertAtBegin                               *
 ***********************************************************************/
bool DntList::insertAtBegin(DntListElement* obj)
{
   if(total == 0)
   {
      /* It's the only element */
      obj->setNext(obj);
      obj->setPrevious(obj);
   }
   else
   {
      /* Insert it before the first */
      obj->setNext(first);
      obj->setPrevious(first->getPrevious());
      obj->getNext()->setPrevious(obj);
      obj->getPrevious()->setNext(obj);
   }

   /* Set as the first */
   first = obj;
   total++;

   return true;
}

/***********************************************************************
 *                               insert                                *
 ***********************************************************************/
bool DntList::insertAtEnd(DntListElement* obj)
{
   if(total == 0)
   {
      /* It's the only element */
      obj->setNext(obj);
      obj->setPrevious(obj);
      first = obj;
   }
   else
   {
      /* Insert it before the first */
      obj->setNext(first);
      obj->setPrevious(first->getPrevious());
      obj->getNext()->setPrevious(obj);
      obj->getPrevious()->setNext(obj);
   }

   total++;

   return true;
}


/***********************************************************************
 *                        removeWithoutDelete                          *
 ***********************************************************************/
bool DntList::removeWithoutDelete(DntListElement* obj)
{
   if(obj == NULL)
   {
      return false;
   }

   /* Update the pointers */
   obj->getNext()->setPrevious(obj->getPrevious());
   obj->getPrevious()->setNext(obj->getNext());
   
   if(first == obj)
   {
      first = first->getNext();
   } 
   
   /* And dec */
   total--;
   if(total == 0)
   {
      first = NULL;
   }

   return true;
}

/***********************************************************************
 *                               remove                                *
 ***********************************************************************/
bool DntList::remove(DntListElement* obj)
{
   if(removeWithoutDelete(obj))
   {
      /* Really remove it */
      delete obj;
      return true;
   }

   return false;
}

/***********************************************************************
 *                               getFirst                              *
 ***********************************************************************/
DntListElement* DntList::getFirst()
{
   return first;
}

/***********************************************************************
 *                                 get                                 *
 ***********************************************************************/
DntListElement* DntList::get(int i)
{
   int j=0;
   DntListElement* e = first;

   if( (i < 0) && (i >= total) )
   {
      return NULL;
   }

   while(j < i)
   {
      e = e->getNext();
      j++;
   }

   return e;
}

/***********************************************************************
 *                               getTotal                              *
 ***********************************************************************/
int DntList::getTotal()
{
   return total;
}

