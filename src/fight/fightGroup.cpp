/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "fightGroup.h"
#include "../engine/collision.h"
#include <SDL2/SDL.h>

/***************************************************************
 *                        CONSTRUCTOR                          *
 ***************************************************************/
fightGroup::fightGroup()
{
   int i;

   /* mark with NULL all pointers */
   for(i=0;i<FIGHT_MAX_CHARACTERS_PER_GROUP;i++)
   {
      Characters[i] = NULL;
   }
   actualCharacters = 0;
}

/***************************************************************
 *                         Empty                               *
 ***************************************************************/
void fightGroup::empty()
{
   int i;

   /* mark with NULL all pointers */
   for(i=0;i<FIGHT_MAX_CHARACTERS_PER_GROUP;i++)
   {
      Characters[i] = NULL;
   }
   actualCharacters = 0;
}

/***************************************************************
 *                        DESTRUCTOR                           *
 ***************************************************************/
fightGroup::~fightGroup()
{
   empty();
   actualCharacters = 0;
}

/***************************************************************
 *                      insertCharacter                        *
 ***************************************************************/
bool fightGroup::insertCharacter(Character* pers)
{
   if(actualCharacters < FIGHT_MAX_CHARACTERS_PER_GROUP)
   {
      /* can insert Character */
      Characters[actualCharacters] = pers;
      pers->currentEnemy = NULL;
      actualCharacters++;
      return(true);
   }
   else
   {
      /* cannot insert Character on this group */
      return(false);
   }
}

/***************************************************************
 *                       isCharacterIn                         *
 ***************************************************************/
bool fightGroup::isCharacterIn(Character* pers)
{
   int i;
   for(i=0;i<actualCharacters;i++)
   {
      if(Characters[i] == pers)
      {
          return(true);
      }
   }
   return(false);
}

/***************************************************************
 *                     getNearestEnemy                         *
 ***************************************************************/
Character* fightGroup::getNearestEnemy(Character* pers)
{
   Character* enemy = NULL;
   float dist = 0;
   float acDist = 0;
   int ch;
   for(ch=0;ch<actualCharacters;ch++)
   {
      if( (Characters[ch]->isAlive()) && (Characters[ch] != pers))
      {
         if(enemy == NULL)
         {
            enemy = Characters[ch];
            acDist = sqrt( 
                (Characters[ch]->scNode->getPosX() - pers->scNode->getPosX()) *
                (Characters[ch]->scNode->getPosX() - pers->scNode->getPosX()) +
                (Characters[ch]->scNode->getPosZ() - pers->scNode->getPosZ()) *
                (Characters[ch]->scNode->getPosZ() - pers->scNode->getPosZ()) );
         }
         else
         {
            dist = sqrt( 
                (Characters[ch]->scNode->getPosX() - pers->scNode->getPosX()) *
                (Characters[ch]->scNode->getPosX() - pers->scNode->getPosX()) +
                (Characters[ch]->scNode->getPosZ() - pers->scNode->getPosZ()) *
                (Characters[ch]->scNode->getPosZ() - pers->scNode->getPosZ()) );
            if(dist < acDist)
            {
               acDist = dist;
               enemy = Characters[ch];
            }
         }
      }
   }

   if( (enemy) && (acDist <= DNT_BATTLE_RANGE*2) )
   {
      /* Verify if enemy is at sight! */
      collision coldet;
      if(coldet.characterAtSight(pers, enemy))
      {
         return(enemy);
      }
   }
   return(NULL);
}

/***************************************************************
 *                      isAnyoneIsAlive                        *
 ***************************************************************/
bool fightGroup::isAnyoneAlive()
{
   int i;
   for(i=0;i<actualCharacters;i++)
   {
      /* Verify status */
      if(Characters[i]->isAlive())
      {
         return(true);
      }
   }
   return(false);
}

/***************************************************************
 *                  anyoneIsAliveAndInRange                    *
 ***************************************************************/
bool fightGroup::anyoneIsAliveAndInRange(bool onlyHostile, float posX, 
      float posZ, float range)
                                         
{ 
   int i;
   for(i=0;i<actualCharacters;i++)
   {
      /* Verify status */
      if( (Characters[i]->isAlive()) && 
          (Characters[i]->atRange(posX, posZ, range)) &&
          ( (!onlyHostile) || 
            (Characters[i]->getPsychoState() == PSYCHO_HOSTILE)) )
      {
         return(true);
      }
   }
   return(false);
}

/***************************************************************
 *                             total                           *
 ***************************************************************/
int fightGroup::total()
{
   return(actualCharacters);
}

/***************************************************************
 *                          getAtPosition                      *
 ***************************************************************/
Character* fightGroup::getAtPosition(int pos)
{
   if( (pos >= 0) && (pos < actualCharacters) )
   {
      return(Characters[pos]);
   }
   return(NULL);
}

