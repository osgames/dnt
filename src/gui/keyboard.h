/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _farso_keyboard_h
#define _farso_keyboard_h

#include <SDL2/SDL.h>


namespace Farso
{

class GuiObject;

/*! The keyboard implementation for FarSO */
class Keyboard
{
   public:

      /*! Update current keyboard state. */
      void updateState();

      /*! Get current keyboard state. */
      const Uint8* getState() {return keys;};

      /*! Tell the controller that we started to edit a text. For example, 
       * when the user changed the focus to a TextBar.
       * \param editor pointer to the GuiObject that will expect the
       *               text editing events to treat. */
      void startEditingText(GuiObject* editor);
      /*! Tell the controller that we stoped to edit a text. For example,
       * when the focus is no more on a TextBar. */
      void stopEditingText();
      /*! \return if is actually editing a text */
      bool isEditingText();

      /*! Update the keyboard by received events. Usually used for text
       * editing only.
       * \param event event received to treat. */
      void updateByEvent(SDL_Event event);

   private:
      static const Uint8 *keys;      /**< current keyboard state */
      static GuiObject* editor;      /**< Object that is editing the text */
};

}


#endif


