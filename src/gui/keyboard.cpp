/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "keyboard.h"
#include "guiobject.h"
#include "textbar.h"

using namespace Farso;

/*****************************************************************
 *                         updateState                           *
 *****************************************************************/
void Keyboard::updateState()
{
   keys = SDL_GetKeyboardState(NULL);
}

/*****************************************************************
 *                      startEditingText                         *
 *****************************************************************/
void Keyboard::startEditingText(GuiObject* editor)
{
   Keyboard::editor = editor;
   SDL_StartTextInput();
}

/*****************************************************************
 *                       stopEditingText                         *
 *****************************************************************/
void Keyboard::stopEditingText()
{
   if(editor != NULL)
   {
      editor = NULL;
      SDL_StopTextInput();
   }
}

/*****************************************************************
 *                        isEditingText                          *
 *****************************************************************/
bool Keyboard::isEditingText()
{
   return editor != NULL;
}

/*****************************************************************
 *                         updateByEvent                         *
 *****************************************************************/
void Keyboard::updateByEvent(SDL_Event event)
{
   switch (event.type) 
   {
      case SDL_TEXTINPUT:
         /* Add new text onto the end of our text */
         if(editor->type == OBJECT_TEXT_BAR)
         {
            TextBar* textBar = (TextBar*) editor;
            textBar->appendToCursor(event.text.text);
         }
      break;
      case SDL_TEXTEDITING:
         //TODO: if we'll support ISM.
      break;
   }

}


/*****************************************************************
 *                        static members                         *
 *****************************************************************/
const Uint8* Keyboard::keys = NULL;
GuiObject* Keyboard::editor = NULL;

