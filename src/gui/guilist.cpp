/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "guilist.h"
#include "menu.h"
#include "tabbox.h"

#include <stdio.h>

#ifdef __APPLE__
   #include <SDL_image/SDL_image.h>
#else
   #include <SDL2/SDL_image.h>
#endif
using namespace std;
using namespace Farso;

/**************************************************************
 *                         Constructor                        *
 **************************************************************/
GuiList::GuiList(int sWidth, int sHeight, SDL_Surface* surface, 
                 bool hasDecor, GuiObject* owner, int t)
{
   this->owner = owner;
   intMenu = NULL;
   wSurface = surface;
   wWidth = sWidth;
   wHeight = sHeight;
   wHasDecor = hasDecor;
   type = t;
   tab = NULL;
}

/**************************************************************
 *                          Destructor                        *
 **************************************************************/
GuiList::~GuiList()
{
   /* Clear the list */
   clearList();

   /* Remove internal menu, if any */
   if(intMenu)
   {
      removeMenu();
   }

   /* Remove internal tab, if any */
   if(tab)
   {
      delete(tab);
   }
}

/**************************************************************
 *                          clearList                         *
 **************************************************************/
void GuiList::clearList()
{
   std::list<GuiObject*>::iterator it;

   for(it=list.begin(); it != list.end(); it++)
   {
      delete *it;
   }
   list.clear();
}

/**************************************************************
 *                            changed                         *
 **************************************************************/
bool GuiList::changed()
{
   std::list<GuiObject*>::iterator it;

   /* Verify all objects on list */
   for(it=list.begin(); it != list.end(); it++)
   {
      if((*it)->changed())
      {
         return(true);
      }
   }

   /* Verify tabBox Objects */
   if(tab != NULL)
   {
      TabBox* tb = (TabBox*)tab;
      if(tb->changed())
      {
         return(true);
      }

      if(tb->getActiveList() != NULL)
      {
         if(tb->getActiveList()->changed())
         {
            return(true);
         }
      }
   }

   return(false);
}

/**************************************************************
 *                            draw                            *
 **************************************************************/
void GuiList::draw()
{ 
   std::list<GuiObject*>::iterator it;
   GuiObject* obj;

   for(it=list.begin(); it != list.end(); it++)
   {
      obj = (*it);
      /* Only draw visible objects */
      if(obj->isVisible())
      {
         switch(obj->type)
         {
            case OBJECT_BUTTON:
            {
                 Button *b = (Button*) obj;   
                 b->draw();
                 break;
            }
            case OBJECT_TEXT_BAR:
            {
                 TextBar *bart = (TextBar*) obj; 
                 bart->draw();
                 break;
            }
            case OBJECT_SEL_BOX:
            {
                 CxSel *cx = (CxSel*) obj;
                 cx->draw();
                 break;
            }
            case OBJECT_SEL_TEXT:
            {
                 SelText *st = (SelText*) obj;
                 st->draw();
                 break;
            }
            case OBJECT_PICTURE:
            {
                 Picture* fig = (Picture*) obj;
                 fig->draw();
                 break;
            }
            case OBJECT_TEXT_BOX:
            {
                 TextBox *quad = (TextBox*) obj;
                 quad->draw();
                 break;
            }
            case OBJECT_TAB_BUTTON:
            {
                 TabButton *bt = (TabButton*) obj; 
                 bt->setCurrent(-1);
                 bt->draw();
                 break;
            }
            case OBJECT_HEALTH_BAR:
            {
                 HealthBar* hb = (HealthBar*) obj;
                 hb->draw();
                 break;
            }
            default:break;
          
         } //case
      }
   }

   /* TabBox Draw */
   if(tab != NULL)
   {
      tab->draw();
   }
}

/**************************************************************
 *                            insert                          *
 **************************************************************/
void GuiList::insert(GuiObject* obj)
{
   /* Define object's parent (our owner) */
   obj->setParent(owner);

   /* really insert it at list */
   if(type == LIST_TYPE_ADD_AT_BEGIN)
   {
      list.push_front(obj);
   }
   else /*if(type == LIST_TYPE_ADD_AT_END)*/
   {
      list.push_back(obj);
   }
}

/**************************************************************
 *                         insertButton                       *
 **************************************************************/
Button* GuiList::insertButton(int xa,int ya,int xb,int yb,
                           string text, bool oval)
{
   Button* novo;
   novo = new Button(xa,ya,xb,yb, text, oval, wSurface, !wHasDecor);
   insert(novo);
   return(novo);
}

/**************************************************************
 *                         insertCxSel                        *
 **************************************************************/
CxSel* GuiList::insertCxSel(int xa,int ya, bool selected)
{
   CxSel* novo;
   novo = new CxSel(xa, ya, wSurface, !wHasDecor);
   novo->setSelection(selected);
   insert(novo);
   return(novo);
}

/**************************************************************
 *                        insertPicture                       *
 **************************************************************/
Picture* GuiList::insertPicture(int x,int y,int w,int h,const char* arquivo)
{
   Picture* novo;
   novo = new Picture(x, y, w, h, arquivo, wSurface, !wHasDecor);
   /* Use alpha from source at empty windows */
   if(!wHasDecor)
   {
      novo->setUseAlphaFromSource();
   }
   insert(novo);
   return(novo);
} 

/**************************************************************
 *                       insertTabButton                      *
 **************************************************************/
TabButton* GuiList::insertTabButton(int x,int y,int w,int h,const char* arquivo)
{
   TabButton* novo;

   if(arquivo)
   {
      novo = new TabButton(x,y,arquivo,wSurface, !wHasDecor);
   }
   else
   {
      novo = new TabButton(x,y,w,h, wSurface, !wHasDecor);
   }
   insert(novo);
   return(novo);
} 

/**************************************************************
 *                        insertTextBar                       *
 **************************************************************/
TextBar* GuiList::insertTextBar(int xa,int ya,int xb,int yb, string text,
                                int cript)
{
   TextBar* novo;
   novo = new TextBar(xa,ya,xb,yb, text, cript, wSurface, !wHasDecor);
   insert(novo);
   return(novo);
} 

/**************************************************************
 *                         insertTextBox                      *
 **************************************************************/
TextBox* GuiList::insertTextBox(int xa,int ya,int xb,int yb,
                                   int frameType, string text)
{
   TextBox* novo;
   novo = new TextBox(xa,ya,xb,yb,frameType,wSurface, !wHasDecor);
   novo->setText(text);
   insert(novo);
   return(novo);
} 

/**************************************************************
 *                        insertSelText                       *
 **************************************************************/
SelText* GuiList::insertSelText(int xa,int ya,int xb,int yb,
                                string text0, string text1,
                                string text2, string text3, 
                                string text4)
{
   SelText* novo;
   novo = new SelText(xa,ya,xb,yb,text0,text1,text2,text3,text4, wSurface, 
                      !wHasDecor);
   insert(novo);
   return(novo);
} 

/**************************************************************
 *                         insertRolBar                       *
 **************************************************************/
RolBar* GuiList::insertRolBar(int xa,int ya,int xb,int yb,string txt)
{
   RolBar* novo;
   novo = new RolBar(xa,ya,xb,yb,txt, this, wSurface, !wHasDecor);
   insert(novo);
   return(novo);
}

/**************************************************************
 *                        insertListText                      *
 **************************************************************/
ListText* GuiList::insertListText(int xa,int ya,int xb,int yb)
{
   ListText* novo;
   novo = new ListText(xa,ya,xb,yb, wSurface, !wHasDecor, this);
   insert(novo);
   return(novo);
}

/**************************************************************
 *                        insertFileSel                       *
 **************************************************************/
FileSel* GuiList::insertFileSel(int xa, int ya, bool load, 
                                string dir, bool nav)
{
   FileSel* n = new FileSel(xa, ya, load, dir, this, wSurface, 
                            !wHasDecor, nav);
   insert(n);
   return(n);
}

/**************************************************************
 *                      insertHealthBar                       *
 **************************************************************/
HealthBar* GuiList::insertHealthBar(int xa, int ya, int xb, int yb, int max)
{
   HealthBar* n = new HealthBar(xa, ya, xb, yb, wSurface, !wHasDecor);
   insert(n);
   n->defineMaxHealth(max);
   return(n);
}

/*********************************************************************
 *                          defineTabBox                             *
 *********************************************************************/
GuiObject* GuiList::defineTabBox(int x1, int y1, int x2, int y2)
{
   if(tab != NULL)
   {
      TabBox* tb = (TabBox*)tab;
      delete(tb);
   }

   tab = (GuiObject*)new TabBox(x1,y1,x2,y2, wSurface, !wHasDecor);
   tab->setParent(owner);
   return(tab);
}

/*********************************************************************
 *                            getTabBox                              *
 *********************************************************************/
GuiObject* GuiList::getTabBox()
{
   return(tab);
}

/*********************************************************************
 *                       getActiveTabBoxList                         *
 *********************************************************************/
GuiList* GuiList::getActiveTabBoxList()
{
   if(tab != NULL)
   {
      TabBox* tb = (TabBox*)tab;
      return(tb->getActiveList());
   }

   return(NULL);
}

/**************************************************************
 *                            addMenu                         *
 **************************************************************/
GuiObject* GuiList::addMenu()
{
   if(intMenu)
   {
      removeMenu();
   }
   intMenu = (GuiObject*) new Menu(0,0,wWidth, wHeight, wSurface, !wHasDecor);
   intMenu->setParent(owner);
   return(intMenu);
}

/**************************************************************
 *                           getMenu                          *
 **************************************************************/
GuiObject* GuiList::getMenu()
{
   return(intMenu);
}

/**************************************************************
 *                          removeMenu                        *
 **************************************************************/
void GuiList::removeMenu()
{
   if(intMenu)
   {
      Menu* men = (Menu*)intMenu;
      delete(men);
   }
   intMenu = NULL;
}


