/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _farso_guiobject_h
#define _farso_guiobject_h

#include <SDL2/SDL.h>
#include <iostream>
#include <string>

#include "keyboard.h"
#include "mouse.h"

namespace Farso
{

/*! The farso guiObject types */
enum FarsoObjectEnum
{
   OBJECT_WINDOW,
   OBJECT_BUTTON,
   OBJECT_TEXT_BAR,
   OBJECT_SEL_BOX,
   OBJECT_PICTURE,
   OBJECT_SEL_TEXT,
   OBJECT_TEXT_BOX,
   OBJECT_MENU_ITEM,
   OBJECT_TAB_BUTTON,
   OBJECT_ROL_BAR,
   OBJECT_LIST_TEXT,
   OBJECT_FILE_SEL,
   OBJECT_HEALTH_BAR,
   OBJECT_TAB_BOX
};

/*! The Class guiObject is a generic interface to all GUI related objects. */
class GuiObject
{
   public:
      int type;            /**< Object Type */ 

      /*! Constructor 
       * \param surface -> SDL_Surface to draw to 
       * \param transparentWindow -> if surface that contains the object is 
       * transparent or with decorations and background. */
      GuiObject(SDL_Surface* surface, bool transparentSurface)
      {
         available=true;
         visible = true;
         text="";
         mouseHint = "";
         wSurface = surface;
         parent = NULL;
         hadChanged = false;
         this->transparentSurface = transparentSurface;
      };

      /*! Virtual Destructor */
      virtual ~GuiObject(){};

      /*! Virtual Draw Function */
      virtual void draw()=0;

      /*! \return Absolute object's X1 coordinate with parents applyed */
      int getAbsoluteX1()
      {
         if(parent != NULL)
         {
            return x1 + parent->getAbsoluteX1();
         }

         return x1;
      }

      /*! \return Absolute object's X2 coordinate with parents applyed */
      int getAbsoluteX2()
      {
         if(parent != NULL)
         {
            return x2 + parent->getAbsoluteX1();
         }

         return x2;
      }

      /*! \return Absolute object's Y1 coordinate with parents applyed */
      int getAbsoluteY1()
      {
         if(parent != NULL)
         {
            return y1 + parent->getAbsoluteY1();
         }

         return y1;
      }

      /*! \return Absolute object's Y2 coordinate with parents applyed */
      int getAbsoluteY2()
      {
         if(parent != NULL)
         {
            return y2 + parent->getAbsoluteY1();
         }

         return y2;
      }

      /*! \return absolute coordinates of the object, obtained by applying
       * parent's coordinates to them */
      void getAbsoluteCoordinates(int& x1, int& y1, int& x2, int& y2) 
      {
         x1 = this->x1;
         y1 = this->y1;
         x2 = this->x2;
         y2 = this->y2;
         if(parent != NULL)
         {
            /* Apply parent's translation origin (px1, py1) */
            int px1=0,py1=0,px2=0,py2=0;
            parent->getAbsoluteCoordinates(px1, py1, px2, py2);
            x1 += px1;
            y1 += py1;
            x2 += px1;
            y2 += py1;
         }
      }

      /*! Verify if the mouse is inner the object or not.
       * \note if object's coordinate is relative to a container,
       *       must pass container's coordinate.
       * \param containerX container of the object X coordinate
       * \param containerY container of the object Y coordinate */
      bool isMouseInner()
      {
         int ax1=0, ay1=0, ax2=0, ay2=0;
         getAbsoluteCoordinates(ax1, ay1, ax2, ay2);
         return mouse.isInner(ax1, ay1, ax2, ay2);
      };

      /*! Gets the object text
       * \return object text */
      std::string getText(){return(text);};

      /*! Set the object text
       * \param txt -> new text */
      void setText(std::string txt){text = txt;};

      /*! Gets the X1 Coordinate
       * \return x1 coordinate */
      int getX1(){return(x1);};

      /*! Gets the X2 Coordinate
       * \return x2 coordinate */
      int getX2(){return(x2);};

      /*! Gets the y1 Coordinate
       * \return y1 coordinate */
      int getY1(){return(y1);};

      /*! Gets the Y2 Coordinate
       * \return y2 coordinate */
      int getY2(){return(y2);};

      /*! Set Object Coordinate
       * \param xa -> x1 coordinate
       * \param ya -> y1 coordinate
       * \param xb -> x2 coordinate
       * \param yb -> y2 coordinate */
      void setCoordinate(int xa, int ya, int xb, int yb)
      {
         x1 = xa; x2 = xb; y1 = ya; y2 = yb;
      };

      /*! Get Object Coordinate
       * \param xa -> x1 coordinate
       * \param ya -> y1 coordinate
       * \param xb -> x2 coordinate
       * \param yb -> y2 coordinate */
      void getCoordinate(int& xa, int& ya, int& xb, int& yb)
      {
         xa = x1; xb = x2; ya = y1; yb = y2;
      };

      /*! Verify if the object is available
       * return true if is avaible */
      bool isAvailable() {return(available);};

      /*! Set if the item is available or not
       * \param av -> true if is available, false otherwise */
      void setAvailable(bool av){available = av;};

      /*! Verify if the draw state changes */
      bool changed(){bool prev = hadChanged; hadChanged = false;return(prev);};

      /*! Set the window as changed */
      void setChanged(){hadChanged = true;};

      /*! Set the surface used */
      void setSurface(SDL_Surface* surface){wSurface = surface;};

      /*! Set Text to display when mouse is over the object */
      void setMouseHint(std::string txt){mouseHint=txt;};
      /*! Get current mouse hint */
      std::string getMouseHint(){return(mouseHint);};
   
      /*! Hide the object */
      void hide(){visible=false;available=false;hadChanged=true;};
      /*! Show the object */
      void show(){visible=true;available=true;hadChanged=true;};
      /*! Get visibility status */
      bool isVisible(){return(visible);};

      /*! \return pointer to parent GuiObject, if any */
      GuiObject* getParent() { return parent;} ;
      /*! Set parent of the current GuiObject */
      void setParent(GuiObject* parent) { this->parent = parent; };

   protected:
      int x1,              /**< Coordinate on Parent */
          y1,              /**< Coordinate on Parent */
          x2,              /**< Coordinate on Parent */
          y2;              /**< Coordinate on Parent */
      bool available;      /**< Available? */
      std::string text;      /**< Text Display */
      std::string mouseHint; /**< Hint when mouse over */
      SDL_Surface* wSurface; /**< Screen surface */
      bool visible;        /**< Toggle object visibility */
      MouseCursor mouse;   /**< Mouse used */
      Keyboard keyboard;   /**< Keyboard used */
      GuiObject* parent;   /**< Parent object - if any */
      bool transparentSurface; /**< If surface container is transparent */
   
   private:
      bool hadChanged;     /**< Flag if the had changed its draw state */
};

}

#endif

