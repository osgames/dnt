/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "textbar.h"
#include <SDL2/SDL.h>
using namespace std;
using namespace Farso;

#define REFRESH_RATE 80

/********************************************************************
 *                          Constructor                             *
 ********************************************************************/
TextBar::TextBar(int xa,int ya,int xb,int yb, string text1, bool cripto,
                 SDL_Surface* screen, bool transparentSurface)
        :GuiObject(screen, transparentSurface)
{
   lastWrite = 0;
   x1 = xa;
   x2 = xb;
   y1 = ya;
   y2 = yb;
   init = 0;
   end = 0;
   cursorPos = 0;
   cursorIndex = 0;
   lastChar='\0';
   cript = cripto;
   type = OBJECT_TEXT_BAR;
   text = text1;
}

/********************************************************************
 *                          Constructor                             *
 ********************************************************************/
TextBar::~TextBar()
{
}

/********************************************************************
 *                            putText                               *
 ********************************************************************/
void TextBar::putText(bool showCursor)
{
   fnt.defineFont(opt.getDefaultFont(), 12);
   fnt.defineFontAlign(Font::ALIGN_LEFT);
   fnt.defineFontStyle(Font::STYLE_NORMAL);

   if(available)
   {
      color_Set(colors.colorCont[2].R, colors.colorCont[2].G,
                colors.colorCont[2].B, colors.colorCont[2].A);
   }
   else
   {
      color_Set(colors.colorCont[0].R, colors.colorCont[0].G,
                colors.colorCont[0].B, colors.colorCont[0].A);
   }
   rectangle_Fill(wSurface,x1+1, y1+1, x2-1, y2-1);

   if(available)
   {
      color_Set(colors.colorCont[1].R, colors.colorCont[1].G,
            colors.colorCont[1].B, colors.colorCont[1].A);
   }
   else
   {
      color_Set(colors.colorText.R, colors.colorText.G,
            colors.colorText.B, colors.colorText.A);
   }

   if(init > (int)cursorIndex)
   {
      /* Current cursor isn't visible, must reset displaying init */
      init = cursorIndex;
   }
   end = (int)text.length();
   string writeText = text.substr(init, end - init);

   int maxWidth = ((x2-1) - (x1+3));

   while( fnt.getStringWidth(writeText, true) > maxWidth )
   {
      if(end > (int) cursorIndex)
      {
         int lastEnd = end;
         end = fnt.getPreviousCharacterStart(writeText, lastEnd);
         writeText.erase(writeText.length() - (lastEnd - end));
      }
      else
      {
         int newInit = fnt.getNextCharacterStart(writeText, 0);
         writeText.erase(0, newInit);
         init += newInit;
      }
   }

   fnt.forceUTF8OnNextWrite();
   fnt.write(wSurface, x1+3, y1+1, writeText);

   if(showCursor)
   {
      /* Define mark position */
      string s = text.substr(init, end).substr(0, cursorIndex - init);
      int x = x1 + 2 + fnt.getStringWidth(s, true);
      line_Draw(wSurface,x,y1+3,x,y2-3);
   }
   setChanged();
}

/********************************************************************
 *                              setText                             *
 ********************************************************************/
void TextBar::setText(string txt)
{
   text = txt;
   draw();
}

/********************************************************************
 *                              draw                                *
 ********************************************************************/
void TextBar::draw()
{
   /* No draw when hidden */
   if(!isVisible())
   {
      return;
   }
   
   color_Set(colors.colorCont[0].R, colors.colorCont[0].G,
             colors.colorCont[0].B, colors.colorCont[0].A);
   rectangle_2Colors(wSurface,x1,y1,x2,y2, colors.colorCont[1].R,
                     colors.colorCont[1].G,colors.colorCont[1].B,
                     colors.colorCont[1].A);
   cursorIndex = 0;
   cursorPos = 0;
   putText(false);
   setChanged();
}

/********************************************************************
 *                    defineCursorPosition                          *
 ********************************************************************/
void TextBar::defineCursorPosition()
{
   fnt.defineFont(opt.getDefaultFont(),12);
   fnt.defineFontAlign(Font::ALIGN_LEFT);
   fnt.defineFontStyle(Font::STYLE_NORMAL);
   string s = text.substr(init,end);

   string saux = "";

   cursorIndex = init;
   if(cursorIndex > 0)
   {
      string toInit = text.substr(0, cursorIndex);
      cursorPos = fnt.getStringLength(toInit);
   }
   else
   {
      cursorPos = 0;
   }
   
   int i = 0;
   int xPos = mouse.getX() - (getAbsoluteX1() + 2);
   int prevIndex;
   int sizeCurChar;

   while( (i < (int)s.length()) && (fnt.getStringWidth(saux, true) < xPos) )
   {
      cursorPos++;
      prevIndex = cursorIndex;
      cursorIndex = fnt.getNextCharacterStart(text, prevIndex);
      sizeCurChar = cursorIndex - prevIndex;
      saux += text.substr(prevIndex, sizeCurChar);
      i += sizeCurChar;
   }
}

/********************************************************************
 *                             Write                                *
 ********************************************************************/
int TextBar::doWrite()
{
   string c;
   c = "";
   const Uint8* keys = keyboard.getState();
   int pronto = 0;
   int ult = SDL_GetTicks();
   bool shouldUpdate = false;

   if(!available)
   {
      keyboard.stopEditingText();
      return 1;
   }


   if(!keyboard.isEditingText())
   {
      keyboard.startEditingText(this);
   }

   /* End Edition Keys */
   if(keys[SDL_SCANCODE_ESCAPE] || 
      keys[SDL_SCANCODE_RETURN] ||
      keys[SDL_SCANCODE_KP_ENTER])
   {
      pronto = 1;
   }

   /* Cursor Movimentation Keys */
   else
   if ( (keys[SDL_SCANCODE_HOME]) && ((ult - lastWrite) >= REFRESH_RATE))
   {
       cursorPos = 0;
       cursorIndex = 0;
       lastWrite = ult;
       shouldUpdate = true;
   }
   else
   if ( (keys[SDL_SCANCODE_END]) && ((ult - lastWrite) >= REFRESH_RATE))
   {
       cursorIndex = (int) text.length();
       cursorPos = fnt.getStringLength(text);
       lastWrite = ult;
       shouldUpdate = true;
   }
   else
   if ((keys[SDL_SCANCODE_RIGHT]) && ((ult - lastWrite) >= REFRESH_RATE))
   {
       if(cursorIndex < text.length())
       {
          cursorIndex = fnt.getNextCharacterStart(text, cursorIndex);
          cursorPos++;
       }
       lastWrite = ult;
       shouldUpdate = true;
   }
   else
   if ((keys[SDL_SCANCODE_LEFT]) && ((ult - lastWrite) >= REFRESH_RATE))
   {
       if(cursorIndex > 0)
       {
          cursorIndex = fnt.getPreviousCharacterStart(text, cursorIndex);
          cursorPos--;
       }
       lastWrite = ult;
       shouldUpdate = true;
   }
   else
      /* Delete Keys */
   if( (keys[SDL_SCANCODE_DELETE]) && ((ult - lastWrite) >= REFRESH_RATE))
   {
       if(cursorIndex < text.length())
       {   
          text.erase(cursorIndex, fnt.getNextCharacterStart(text, cursorIndex));
       }
       lastWrite = ult;
       shouldUpdate = true;
   }
   else
      /* Delete backwards keys */
   if((keys[SDL_SCANCODE_BACKSPACE]) && ((ult - lastWrite) >= REFRESH_RATE))
   {
      if(cursorIndex > 0)
      {
         int indexOfPrev = fnt.getPreviousCharacterStart(text, cursorIndex);
         text.erase(indexOfPrev, cursorIndex - indexOfPrev);
         cursorIndex = indexOfPrev;
      }
      lastWrite = ult;
      shouldUpdate = true;
   }

   if(mouse.getButtonState() & SDL_BUTTON(1))
   {
      if(isMouseInner())
      {
         /* Calculate the New Position */
         defineCursorPosition();
         putText(true);
      }
      else
      {
         pronto = 1;
      }
   }

  if(!pronto)
  {
     if(shouldUpdate)
     {
        putText(true);
     }
  }
  else
  {
     cursorIndex = 0;
     cursorPos = 0;
     putText(false);
     keyboard.stopEditingText();
  }

  return pronto;
}

/********************************************************************
 *                          appendToCursor                          *
 ********************************************************************/
void TextBar::appendToCursor(std::string textToAppend)
{
   text.insert(cursorIndex, textToAppend);
   cursorPos += fnt.getStringLength(textToAppend);
   cursorIndex += textToAppend.length();
   putText(true);
}



