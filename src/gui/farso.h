/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _farso_h
#define _farso_h


#include <SDL2/SDL.h>
#include "draw.h"
#include "button.h"
#include "color.h"
#include "cxsel.h"
#include "window.h"
#include "interface.h"
#include "dntfont.h"
#include "healthBar.h"
#include "textbox.h"
#include "rolbar.h"
#include "seltext.h"
#include "textbar.h"
#include "guiobject.h"
#include "guilist.h"
#include "picture.h"
#include "menu.h"
#include "mouse.h"
#include "keyboard.h"
#include "tabbutton.h"
#include "messages.h"
#include "farsoopts.h"

namespace Farso
{

/*! Farso is the SDL glue and GUI controller used by DNT. It is the main 
 * access point to init SDL2 and OpenGL context for the game itself. */
class Farso
{
   public:
      /*! Constructor */
      Farso();
      /*! Destructor */
      ~Farso();

      /*! Init the SDL/OpenGL Context
       * \param title -> title of the screen
       * \param width -> screen width
       * \param height -> screen height
       * \param fullScreen -> true if is fullscreen
       * \param antiAliasingSample -> number of Antialising used (0 == disabled)
       * \param stencilBufferSize -> number of stencil buffers */
      void init(std::string title, int width, int height,
            bool fullScreen, int antiAliasingSample, int stencilBufferSize);

      /* Finish the use of farso. */
      void finish();
 
      /*! Update current input state, mainly mouse and keyboard related ones */
      void updateInputState();

      /*! Swap the buffer and render current frame */
      void swapBuffers();

      /*! Do the clear to the screen (glClear) with needed flags */
      void clearScreen();

      /*! Update the actual view frustum */
      void updateFrustum();

      /*! Refeine the viewport after a update to the window.
       * \param actualFarView far view plane position. */
      void redefineWindowAndViewport(float actualFarView);

      /*! Raycast the x,y point from screen to the world. The Z from screen
       * is obtained by the current Z buffer for x,y point.
       * \param x screen X coordinate.
       * \param y screen Y coordinate.
       * \param worldX pointer to variable to receive world X coordinate
       * \param worldY pointer to variable to receive world Y coordinate
       * \param worldZ pointer to variable to receive world Z coordinate */
      void raycast(GLfloat x, GLfloat y, GLdouble* worldX, GLdouble* worldY, 
            GLdouble* worldZ);

      /*! Raycast the x,y,z point from screen to the world.
       * \param x screen X coordinate.
       * \param y screen Y coordinate.
       * \param z screen Z coordinate.
       * \param worldX pointer to variable to receive world X coordinate
       * \param worldY pointer to variable to receive world Y coordinate
       * \param worldZ pointer to variable to receive world Z coordinate */
      void raycast(GLfloat x, GLfloat y, GLfloat z,
            GLdouble* worldX, GLdouble* worldY, GLdouble* worldZ);

      /*! Map world coordinates to screen coordinates.
       * \param worldX world X coordinate
       * \param worldY world Y coordinate
       * \param worldZ world Z coordinate
       * \param wX resulting window X coordinate
       * \param wY resulting window Y coordinate
       * \param wZ resulting window Z coordinate */
      void project(GLdouble worldX, GLdouble worldY, GLdouble worldZ, 
                   GLdouble* wX, GLdouble* wY, GLdouble* wZ);

      /*! Verify if the cube has some part inner current view frustum or not.
       * This function is usually used for view frustum culling.
       * \param x1 -> upper X coordinate
       * \param y1 -> upper Y coordinate
       * \param z1 -> upper Z coordinate
       * \param x2 -> lower X coordinate
       * \param y2 -> lower Y coordinate
       * \param z2 -> lower Z coordinate  
       * \return if visible or not */
      bool visibleCube(GLfloat x1, GLfloat y1, GLfloat z1,
            GLfloat x2, GLfloat y2, GLfloat z2);

      /*! \return #window */
      SDL_Window* getWindow() { return window; };
      /*! \return #mainGuiInterface */
      GuiInterface* getMainGuiInterface() { return mainGuiInterface; };

      /*! \return current model view matrix (GLdouble[16]) */
      const GLdouble* getModelView() { return modl;};

      /*! Disable the view frustum culling */
      void disableCulling() { cullingEnabled = false; };
      /*! Enable the view frustum culling (enabled by default) */
      void enableCulling() { cullingEnabled = true; };

   protected:
      /*! Define the SDL/OpenGL Context Resolution
       * \param title -> title of the screen
       * \param width -> screen width
       * \param height -> screen height
       * \param fullScreen -> true if is fullscreen
       * \param antiAliasingSample -> number of Antialising used 
       *                              (0 == disabled) */
      void defineResolution(std::string title, int width, int height,
            bool fullScreen, int antiAliasingSample, int stencilBufferSize);

   private:

      /*! The window used */
      static SDL_Window* window;
      /*! Our OpenGL context */
      static SDL_GLContext glcontext;
      /*! The main GUI interface used on the application */
      static GuiInterface* mainGuiInterface;

      /*! If farso was inited */
      static bool inited;

      static GLdouble proj[16];             /**< Projection Matrix */
      static GLdouble modl[16];             /**< ModelView Matrix  */
      static GLfloat camProj[16];           /**< Camera Projection */
      static GLint viewPort[4];             /**< ViewPort Matrix */
      static GLfloat** visibleMatrix;       /**< Current visible matrix */
      static bool usingStencilBuffer;       /**< If using stencil buffer*/
      static MouseCursor mouseCursor;       /**< Mouse cursor used */
      static Keyboard keyboard;             /**< Keyboard used */
      static bool cullingEnabled;  /**< If view frustum culling is enabled */

};

}

#endif

