/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "farso.h"
#include "mouse.h"

#include <iostream>
using namespace std;

namespace Farso
{

/*! Delta value to apply to view frustum check, since antialiasing
 * will cause a "glitch" if only render when just visible (ie: we must prepare
 * to render when it will be 'almost visible', or 'potentially visible on
 * next frames'). */
#define ANTI_ALIASING_DELTA  20  


/************************************************************
 *                       Constructor                        *
 ************************************************************/
Farso::Farso()
{
}
Farso::~Farso()
{
}

/************************************************************
 *                        Farso_Init                        *
 ************************************************************/
void Farso::init(string title, int width, int height,
          bool fullScreen, int antiAliasingSamples, int stencilBufferSize)
{
   /* Start Openning the screen  */
   if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) 
   {
      cerr << "Argh! Can't init SDL!" << endl;
      exit(1);
   }

   usingStencilBuffer = stencilBufferSize > 0;

   /* Define the resolution */
   defineResolution(title, width, height, fullScreen, antiAliasingSamples,
         stencilBufferSize);

   if( (!inited) && (window != NULL) )
   {
      inited = true;

      /* Alloc the visible Matrix */
      visibleMatrix = new GLfloat*[6];
      for(int i = 0; i < 6; i++)
      {
         visibleMatrix[i] = new GLfloat[4];
         for(int j = 0; j < 4; j++)
         {
            visibleMatrix[i][j] = 0.0f;
         }
      }
      glcontext = SDL_GL_CreateContext(window);

      Font fnt;
      fnt.init();

      mouseCursor.init();

      Colors cor;
      cor.init();

      mainGuiInterface = new GuiInterface("");

   }
}

/************************************************************
 *                  Farso_DefineResolution                  *
 ************************************************************/
void Farso::defineResolution(string title, int width, int height, bool
      fullScreen, int antiAliasingSamples, int stencilBufferSize) 
{
   Options opts;
   SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

   if(stencilBufferSize > 0)
   {
      SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, stencilBufferSize);
   }

   if(antiAliasingSamples > 0)
   {
      SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
      SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, antiAliasingSamples);
   }

   int flags = SDL_WINDOW_OPENGL;

   if(fullScreen)
   {
      flags |= SDL_WINDOW_FULLSCREEN;
   }

   opts.setCurrentScreen(width, height);
   SCREEN_X = width;
   SCREEN_Y = height;

   window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED,
         SDL_WINDOWPOS_UNDEFINED, SCREEN_X, SCREEN_Y, 
         flags);
   if(window == NULL) 
   {
      cerr << "Oxi! Can't ajust video mode: " << SDL_GetError() << endl;

      if(stencilBufferSize > 8)
      {
         cerr << "Trying again with lesser stencil." << endl;
         /* Quit the SDL  */
         SDL_Quit();
         /* Restart it  */
         init(title, width, height, fullScreen, antiAliasingSamples, 8);
      }
      else if(antiAliasingSamples > 0)
      {
         cerr << "Trying again without AntiAliasing." << endl;
         /* Quit the SDL  */
         SDL_Quit();
         /* Restart it  */
         init(title, width, height, fullScreen, 0, stencilBufferSize);
      }
      else
      {
         cerr << "Something must be wrong! " << endl
            << "Try editing ~/.dnt/options.cfg" << endl;
         exit(2);
      }
   }
}

/************************************************************
 *                         Farso_End                        *
 ************************************************************/
void Farso::finish()
{
   /* Clear the visibleMatrix */
   for(int i = 0; i < 6; i++)
   {
      delete[] visibleMatrix[i];
   }
   delete[] visibleMatrix;

   if(inited)
   {
      Font fnt;
      fnt.end();

      mouseCursor.finish();

      delete mainGuiInterface;
      inited = false;
   }


   if(window != NULL)
   {
      SDL_GL_DeleteContext(glcontext);
      SDL_DestroyWindow(window);

      window = NULL;
   }

   SDL_Quit();
}

/************************************************************
 *                         swapBuffers                      *
 ************************************************************/
void Farso::swapBuffers()
{
   SDL_GL_SwapWindow(window);
}

/********************************************************************
 *                            clearScreen                           *
 ********************************************************************/
void Farso::clearScreen()
{
   if(usingStencilBuffer)
   {
      glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT |
            GL_STENCIL_BUFFER_BIT);
   }
   else
   {
      glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
   }
}

/*********************************************************
 *               redefineWindowAndViewport               *
 *********************************************************/
void Farso::redefineWindowAndViewport(float actualFarView)
{
   glViewport(0, 0, SCREEN_X, SCREEN_Y);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(45.0f, SCREEN_X / (float)SCREEN_Y, 1.0f,
         actualFarView);
   glGetIntegerv(GL_VIEWPORT, viewPort);
   glGetFloatv(GL_MODELVIEW_MATRIX, camProj);
   glMatrixMode (GL_MODELVIEW);
   glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

   glEnable(GL_LINE_SMOOTH);
   glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
   glLineWidth(1.5f);
}

/*********************************************************
 *                       raycast                         *
 *********************************************************/
void Farso::raycast(GLfloat x, GLfloat y, 
      GLdouble* worldX, GLdouble* worldY, GLdouble* worldZ)
{
   /* Define Mouse OpenGL Window Coordinate */
   GLfloat wx = x;
   GLfloat wy = SCREEN_Y - y;
   GLfloat wz = 1;

   /* Get the Zbuffer position of screen x,y */
   glReadPixels((int) wx,(int) wy, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &wz);

   /* Get the world coordinate of the screen position */
   gluUnProject(wx, wy, wz, modl, proj, viewPort, worldX, worldY, worldZ);
}

/*********************************************************
 *                       raycast                         *
 *********************************************************/
void Farso::raycast(GLfloat x, GLfloat y, GLfloat z,
      GLdouble* worldX, GLdouble* worldY, GLdouble* worldZ)
{
   GLfloat wx = x;
   GLfloat wy = SCREEN_Y - y;
   GLfloat wz = z;

   gluUnProject(wx, wy, wz, modl, proj, viewPort, worldX, worldY, worldZ);
}

/*********************************************************
 *                       project                         *
 *********************************************************/
void Farso::project(GLdouble worldX, GLdouble worldY, GLdouble worldZ, 
                    GLdouble* wX, GLdouble* wY, GLdouble* wZ)
{
   gluProject(worldX, worldY, worldZ, modl, proj, viewPort, wX, wY, wZ);
}

/*********************************************************
 *                      updateFrustum                    *
 *********************************************************/
void Farso::updateFrustum()
{
   GLfloat clip[16];
	GLfloat t;

	// Get The Current PROJECTION Matrix From OpenGL
	glGetDoublev( GL_PROJECTION_MATRIX, proj );

	// Get The Current MODELVIEW Matrix From OpenGL
	glGetDoublev( GL_MODELVIEW_MATRIX, modl );

	// Combine The Two Matrices (Multiply Projection By Modelview)
	clip[0] = modl[0] * proj[0] + modl[1] * proj[4] + 
             modl[2] * proj[8] + modl[3] * proj[12];
	clip[1] = modl[0] * proj[1] + modl[1] * proj[5] + 
             modl[2] * proj[9] + modl[3] * proj[13];
	clip[2] = modl[0] * proj[2] + modl[1] * proj[6] + 
             modl[2] * proj[10] + modl[3] * proj[14];
	clip[3] = modl[0] * proj[3] + modl[1] * proj[7] + 
             modl[2] * proj[11] + modl[3] * proj[15];

	clip[4] = modl[4] * proj[0] + modl[5] * proj[4] + 
             modl[6] * proj[8] + modl[7] * proj[12];
	clip[5] = modl[4] * proj[1] + modl[5] * proj[5] + 
             modl[6] * proj[9] + modl[7] * proj[13];
	clip[6] = modl[4] * proj[2] + modl[5] * proj[6] + 
             modl[6] * proj[10] + modl[7] * proj[14];
	clip[7] = modl[4] * proj[3] + modl[5] * proj[7] + 
             modl[6] * proj[11] + modl[7] * proj[15];

	clip[8] = modl[8] * proj[0] + modl[9] * proj[4] + 
             modl[10] * proj[8] + modl[11] * proj[12];
	clip[9] = modl[8] * proj[1] + modl[9] * proj[5] + 
             modl[10] * proj[9] + modl[11] * proj[13];
	clip[10] = modl[8] * proj[2] + modl[9] * proj[6] + 
              modl[10] * proj[10] + modl[11] * proj[14];
	clip[11] = modl[8] * proj[3] + modl[9] * proj[7] + 
              modl[10] * proj[11] + modl[11] * proj[15];

	clip[12] = modl[12] * proj[0] + modl[13] * proj[4] + 
              modl[14] * proj[8] + modl[15] * proj[12];
	clip[13] = modl[12] * proj[1] + modl[13] * proj[5] + 
              modl[14] * proj[9] + modl[15] * proj[13];
	clip[14] = modl[12] * proj[2] + modl[13] * proj[6] + 
              modl[14] * proj[10] + modl[15] * proj[14];
	clip[15] = modl[12] * proj[3] + modl[13] * proj[7] + 
              modl[14] * proj[11] + modl[15] * proj[15];

	// Extract The Numbers For The RIGHT Plane
   visibleMatrix[0][0] = clip[3] - clip[0];
	visibleMatrix[0][1] = clip[7] - clip[4];
	visibleMatrix[0][2] = clip[11] - clip[8];
	visibleMatrix[0][3] = clip[15] - clip[12];

	// Normalize The Result
	t = GLfloat(sqrt( visibleMatrix[0][0] * visibleMatrix[0][0] + 
                     visibleMatrix[0][1] * visibleMatrix[0][1] + 
                     visibleMatrix[0][2] * visibleMatrix[0][2] ));
	visibleMatrix[0][0] /= t;
	visibleMatrix[0][1] /= t;
	visibleMatrix[0][2] /= t;
	visibleMatrix[0][3] /= t;

	// Extract The Numbers For The LEFT Plane
	visibleMatrix[1][0] = clip[3] + clip[0];
	visibleMatrix[1][1] = clip[7] + clip[4];
	visibleMatrix[1][2] = clip[11] + clip[8];
	visibleMatrix[1][3] = clip[15] + clip[12];

	// Normalize The Result
	t = GLfloat(sqrt( visibleMatrix[1][0] * visibleMatrix[1][0] + 
                     visibleMatrix[1][1] * visibleMatrix[1][1] + 
                     visibleMatrix[1][2] * visibleMatrix[1][2] ));
	visibleMatrix[1][0] /= t;
	visibleMatrix[1][1] /= t;
	visibleMatrix[1][2] /= t;
	visibleMatrix[1][3] /= t;

	// Extract The BOTTOM Plane
	visibleMatrix[2][0] = clip[3] + clip[1];
	visibleMatrix[2][1] = clip[7] + clip[5];
	visibleMatrix[2][2] = clip[11] + clip[9];
	visibleMatrix[2][3] = clip[15] + clip[13];

	// Normalize The Result
	t = GLfloat(sqrt( visibleMatrix[2][0] * visibleMatrix[2][0] + 
                     visibleMatrix[2][1] * visibleMatrix[2][1] + 
                     visibleMatrix[2][2] * visibleMatrix[2][2] ));
	visibleMatrix[2][0] /= t;
	visibleMatrix[2][1] /= t;
	visibleMatrix[2][2] /= t;
	visibleMatrix[2][3] /= t;

	// Extract The TOP Plane
	visibleMatrix[3][0] = clip[3] - clip[1];
	visibleMatrix[3][1] = clip[7] - clip[5];
	visibleMatrix[3][2] = clip[11] - clip[9];
	visibleMatrix[3][3] = clip[15] - clip[13];

	// Normalize The Result
	t = GLfloat(sqrt( visibleMatrix[3][0] * visibleMatrix[3][0] + 
                     visibleMatrix[3][1] * visibleMatrix[3][1] + 
                     visibleMatrix[3][2] * visibleMatrix[3][2] ));
	visibleMatrix[3][0] /= t;
	visibleMatrix[3][1] /= t;
	visibleMatrix[3][2] /= t;
	visibleMatrix[3][3] /= t;

	// Extract The FAR Plane
	visibleMatrix[4][0] = clip[3] - clip[2];
	visibleMatrix[4][1] = clip[7] - clip[6];
	visibleMatrix[4][2] = clip[11] - clip[10];
	visibleMatrix[4][3] = clip[15] - clip[14];

	// Normalize The Result
	t = GLfloat(sqrt( visibleMatrix[4][0] * visibleMatrix[4][0] + 
                     visibleMatrix[4][1] * visibleMatrix[4][1] + 
                     visibleMatrix[4][2] * visibleMatrix[4][2] ));
	visibleMatrix[4][0] /= t;
	visibleMatrix[4][1] /= t;
	visibleMatrix[4][2] /= t;
	visibleMatrix[4][3] /= t;

	// Extract The NEAR Plane
	visibleMatrix[5][0] = clip[3] + clip[2];
	visibleMatrix[5][1] = clip[7] + clip[6];
	visibleMatrix[5][2] = clip[11] + clip[10];
	visibleMatrix[5][3] = clip[15] + clip[14];

	// Normalize The Result
	t = GLfloat(sqrt( visibleMatrix[5][0] * visibleMatrix[5][0] + 
                     visibleMatrix[5][1] * visibleMatrix[5][1] + 
                     visibleMatrix[5][2] * visibleMatrix[5][2] ));
	visibleMatrix[5][0] /= t;
	visibleMatrix[5][1] /= t;
	visibleMatrix[5][2] /= t;
	visibleMatrix[5][3] /= t;

};

/*********************************************************
 *                      visibleCube                      *
 *********************************************************/
bool Farso::visibleCube(GLfloat xa, GLfloat ya, GLfloat za,
      GLfloat xb, GLfloat yb, GLfloat zb)
{
   if(!cullingEnabled)
   {
      /* No culling => always visible. */
      return true;
   }

   /* Apply AntiAliasing Delta */
   GLfloat x1 = xa - ANTI_ALIASING_DELTA;
   GLfloat y1 = ya - ANTI_ALIASING_DELTA;
   GLfloat z1 = za - ANTI_ALIASING_DELTA;
   GLfloat x2 = xb + ANTI_ALIASING_DELTA;
   GLfloat y2 = yb + ANTI_ALIASING_DELTA;
   GLfloat z2 = zb + ANTI_ALIASING_DELTA;

   /* If all points are outter planes, is out! */
   if(((visibleMatrix[0][0] * x1 + visibleMatrix[0][1] * y1 +
        visibleMatrix[0][2] * z1 + visibleMatrix[0][3] <= 0) &&
       (visibleMatrix[0][0] * x1 + visibleMatrix[0][1] * y1 + 
        visibleMatrix[0][2] * z2 + visibleMatrix[0][3] <= 0) &&
       (visibleMatrix[0][0] * x1 + visibleMatrix[0][1] * y2 +
        visibleMatrix[0][2] * z1 + visibleMatrix[0][3] <= 0) &&
       (visibleMatrix[0][0] * x1 + visibleMatrix[0][1] * y2 + 
        visibleMatrix[0][2] * z2 + visibleMatrix[0][3] <= 0) &&
       (visibleMatrix[0][0] * x2 + visibleMatrix[0][1] * y1 + 
        visibleMatrix[0][2] * z1 + visibleMatrix[0][3] <= 0) &&
       (visibleMatrix[0][0] * x2 + visibleMatrix[0][1] * y1 + 
        visibleMatrix[0][2] * z2 + visibleMatrix[0][3] <= 0) &&
       (visibleMatrix[0][0] * x2 + visibleMatrix[0][1] * y2 + 
        visibleMatrix[0][2] * z1 + visibleMatrix[0][3] <= 0) &&
       (visibleMatrix[0][0] * x2 + visibleMatrix[0][1] * y2 + 
        visibleMatrix[0][2] * z2 + visibleMatrix[0][3] <= 0)) ||
      ((visibleMatrix[1][0] * x1 + visibleMatrix[1][1] * y1 + 
        visibleMatrix[1][2] * z1 + visibleMatrix[1][3] <= 0) &&
       (visibleMatrix[1][0] * x1 + visibleMatrix[1][1] * y1 + 
        visibleMatrix[1][2] * z2 + visibleMatrix[1][3] <= 0) &&
       (visibleMatrix[1][0] * x1 + visibleMatrix[1][1] * y2 + 
        visibleMatrix[1][2] * z1 + visibleMatrix[1][3] <= 0) &&
       (visibleMatrix[1][0] * x1 + visibleMatrix[1][1] * y2 + 
        visibleMatrix[1][2] * z2 + visibleMatrix[1][3] <= 0) &&
       (visibleMatrix[1][0] * x2 + visibleMatrix[1][1] * y1 + 
        visibleMatrix[1][2] * z1 + visibleMatrix[1][3] <= 0) &&
       (visibleMatrix[1][0] * x2 + visibleMatrix[1][1] * y1 + 
        visibleMatrix[1][2] * z2 + visibleMatrix[1][3] <= 0) &&
       (visibleMatrix[1][0] * x2 + visibleMatrix[1][1] * y2 +
        visibleMatrix[1][2] * z1 + visibleMatrix[1][3] <= 0) &&
       (visibleMatrix[1][0] * x2 + visibleMatrix[1][1] * y2 + 
        visibleMatrix[1][2] * z2 + visibleMatrix[1][3] <= 0)) ||
      ((visibleMatrix[2][0] * x1 + visibleMatrix[2][1] * y1 + 
        visibleMatrix[2][2] * z1 + visibleMatrix[2][3] <= 0) &&
       (visibleMatrix[2][0] * x1 + visibleMatrix[2][1] * y1 + 
        visibleMatrix[2][2] * z2 + visibleMatrix[2][3] <= 0) &&
       (visibleMatrix[2][0] * x1 + visibleMatrix[2][1] * y2 + 
        visibleMatrix[2][2] * z1 + visibleMatrix[2][3] <= 0) &&
       (visibleMatrix[2][0] * x1 + visibleMatrix[2][1] * y2 + 
        visibleMatrix[2][2] * z2 + visibleMatrix[2][3] <= 0) &&
       (visibleMatrix[2][0] * x2 + visibleMatrix[2][1] * y1 + 
        visibleMatrix[2][2] * z1 + visibleMatrix[2][3] <= 0) &&
       (visibleMatrix[2][0] * x2 + visibleMatrix[2][1] * y1 + 
        visibleMatrix[2][2] * z2 + visibleMatrix[2][3] <= 0) &&
       (visibleMatrix[2][0] * x2 + visibleMatrix[2][1] * y2 + 
        visibleMatrix[2][2] * z1 + visibleMatrix[2][3] <= 0) &&
       (visibleMatrix[2][0] * x2 + visibleMatrix[2][1] * y2 + 
        visibleMatrix[2][2] * z2 + visibleMatrix[2][3] <= 0)) ||
      ((visibleMatrix[3][0] * x1 + visibleMatrix[3][1] * y1 + 
        visibleMatrix[3][2] * z1 + visibleMatrix[3][3] <= 0) &&
       (visibleMatrix[3][0] * x1 + visibleMatrix[3][1] * y1 + 
        visibleMatrix[3][2] * z2 + visibleMatrix[3][3] <= 0) &&
       (visibleMatrix[3][0] * x1 + visibleMatrix[3][1] * y2 + 
        visibleMatrix[3][2] * z1 + visibleMatrix[3][3] <= 0) &&
       (visibleMatrix[3][0] * x1 + visibleMatrix[3][1] * y2 + 
        visibleMatrix[3][2] * z2 + visibleMatrix[3][3] <= 0) &&
       (visibleMatrix[3][0] * x2 + visibleMatrix[3][1] * y1 + 
        visibleMatrix[3][2] * z1 + visibleMatrix[3][3] <= 0) &&
       (visibleMatrix[3][0] * x2 + visibleMatrix[3][1] * y1 + 
        visibleMatrix[3][2] * z2 + visibleMatrix[3][3] <= 0) &&
       (visibleMatrix[3][0] * x2 + visibleMatrix[3][1] * y2 + 
        visibleMatrix[3][2] * z1 + visibleMatrix[3][3] <= 0) &&
       (visibleMatrix[3][0] * x2 + visibleMatrix[3][1] * y2 + 
        visibleMatrix[3][2] * z2 + visibleMatrix[3][3] <= 0)) ||
      ((visibleMatrix[4][0] * x1 + visibleMatrix[4][1] * y1 + 
        visibleMatrix[4][2] * z1 + visibleMatrix[4][3] <= 0) &&
       (visibleMatrix[4][0] * x1 + visibleMatrix[4][1] * y1 + 
        visibleMatrix[4][2] * z2 + visibleMatrix[4][3] <= 0) &&
       (visibleMatrix[4][0] * x1 + visibleMatrix[4][1] * y2 + 
        visibleMatrix[4][2] * z1 + visibleMatrix[4][3] <= 0) &&
       (visibleMatrix[4][0] * x1 + visibleMatrix[4][1] * y2 + 
        visibleMatrix[4][2] * z2 + visibleMatrix[4][3] <= 0) &&
       (visibleMatrix[4][0] * x2 + visibleMatrix[4][1] * y1 + 
        visibleMatrix[4][2] * z1 + visibleMatrix[4][3] <= 0) &&
       (visibleMatrix[4][0] * x2 + visibleMatrix[4][1] * y1 + 
        visibleMatrix[4][2] * z2 + visibleMatrix[4][3] <= 0) &&
       (visibleMatrix[4][0] * x2 + visibleMatrix[4][1] * y2 + 
        visibleMatrix[4][2] * z1 + visibleMatrix[4][3] <= 0) &&
       (visibleMatrix[4][0] * x2 + visibleMatrix[4][1] * y2 + 
        visibleMatrix[4][2] * z2 + visibleMatrix[4][3] <= 0)) ||
      ((visibleMatrix[5][0] * x1 + visibleMatrix[5][1] * y1 + 
        visibleMatrix[5][2]  *z1 + visibleMatrix[5][3] <= 0) &&
       (visibleMatrix[5][0] * x1 + visibleMatrix[5][1] * y1 + 
        visibleMatrix[5][2] * z2 + visibleMatrix[5][3] <= 0) &&
       (visibleMatrix[5][0] * x1 + visibleMatrix[5][1] * y2 + 
        visibleMatrix[5][2] * z1 + visibleMatrix[5][3] <= 0) &&
       (visibleMatrix[5][0] * x1 + visibleMatrix[5][1] * y2 + 
        visibleMatrix[5][2] * z2 + visibleMatrix[5][3] <= 0) &&
       (visibleMatrix[5][0] * x2 + visibleMatrix[5][1] * y1 + 
        visibleMatrix[5][2] * z1 + visibleMatrix[5][3] <= 0) &&
       (visibleMatrix[5][0] * x2 + visibleMatrix[5][1] * y1 + 
        visibleMatrix[5][2] * z2 + visibleMatrix[5][3] <= 0) &&
       (visibleMatrix[5][0] * x2 + visibleMatrix[5][1] * y2 + 
        visibleMatrix[5][2] * z1 + visibleMatrix[5][3] <= 0) &&
       (visibleMatrix[5][0] * x2 + visibleMatrix[5][1] * y2 + 
        visibleMatrix[5][2] * z2 + visibleMatrix[5][3] <= 0)))
   {
      return false;
   }
   else
   {
      /* if a point is inner, so its visible */
      return true;
   }
}

/***************************************************************************
 *                            updateInputState                             *
 ***************************************************************************/
void Farso::updateInputState()
{
   SDL_PumpEvents();
   
   /* Get Keyboard State */
   keyboard.updateState();

   /* Update mouse state and positions */
   mouseCursor.updatePosition();

   /* Let's update things by envents */
   SDL_Event event;
   while(SDL_PollEvent(&event))
   {
      mouseCursor.updateByEvent(event);
      if(keyboard.isEditingText())
      {
         keyboard.updateByEvent(event);
      }
   }
}

/***************************************************************************
 *                            static members                               *
 ***************************************************************************/
SDL_Window* Farso::window = NULL;
SDL_GLContext Farso::glcontext;
GuiInterface* Farso::mainGuiInterface = NULL;
bool Farso::inited = false;
GLdouble Farso::proj[16];
GLdouble Farso::modl[16];
GLfloat Farso::camProj[16];
GLint Farso::viewPort[4];
GLfloat** Farso::visibleMatrix = NULL;
bool Farso::usingStencilBuffer = false;
MouseCursor Farso::mouseCursor;
Keyboard Farso::keyboard;
bool Farso::cullingEnabled = true;

}

