/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _farso_mouse_h
#define _farso_mouse_h

#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL.h>
#ifdef __APPLE__
   #include <SDL_image/SDL_image.h>
#else
   #include <SDL2/SDL_image.h>
#endif

#include <string>
#include <list>

namespace Farso
{

#define MOUSE_CURSOR_NONE         "None"
#define MOUSE_CURSOR_USER_IMAGE   "User"

#define MOUSE_CURSOR_MAX_TEXT_OVER_TIME  220   /**< After 60ms if no update */

#define MAX_MOUSE_BUTTONS    3

/*! A single mouse cursor information */
class SingleMouseCursor
{
   public:
      /*! Constructor */
      SingleMouseCursor();
      /*! Destructor */
      ~SingleMouseCursor();

      GLuint texture;          /**< Cursor texture */
      GLfloat size[2];         /**< Cursor Size */
      GLfloat prop[2];         /**< Cursor Proportion */
      GLfloat hotSpot[2];      /**< Cursor Active Position ("HotSpot") */
      GLfloat scaleFactor;     /**< Cursor Scale Factor */
      std::string fileName;    /**< Cursor FileName ("none" for using image) */
};

class Farso;

class MouseCursor
{
   public:
      /*! cursor Constructor */
      MouseCursor();
      /*! cursor Destructor */
      ~MouseCursor();

      /*! init the cursor use 
       * \note must be called at engine init */
      void init();
      /*! Finish the cursor use
       * \note must be called at engine end*/
      void finish();

      /*! Set the current cursor to one defined by the image file name
       * \param imageFile -> cursor image file name
       * \param spotX -> cursor active X 
       * \param spotY -> cursor active Y
       * \note -> if not yet loaded, will load it to current cursors list */
      void set(std::string imageFile, int spotX, int spotY);

      /*! Set the current mouse Cursor to an image
       * \param img -> sdl surface to set as cursor */
      void set(SDL_Surface* img);

      /*! Draw the Cursor to screen
       * \param angle -> rotation angle to apply to mouse cursor
       * \param scaleX -> X scale factor to apply
       * \param scaleY -> Y scale factor to apply
       * \param scaleZ -> Z scale factor to apply */
      void draw(float angle = 0.0f,
                float scaleX=1.0f, float scaleY=1.0f, float scaleZ=1.0f);

      /*! Set the text over the cursor
       * \param txt -> text to show or "" for no text */
      void setTextOver(std::string txt);

      /*! Hide the mouse cursor (must reset it to cursor to show again) */
      void hide();

      /*! Get the current cursor */
      std::string get();

      /*! Set the font used for text over
       * \param s -> font file name  */
      void setTextOverFont(std::string f);

      /*! Verify if current mouse coordinate is inner a defined screen area.
       * \param x1 -> x1 coordinate
       * \param y1 -> y1 coordinate
       * \param x2 -> x2 coordinate
       * \param y2 -> y2 coordinate
       * \return true if inner area, false otherwise */
      bool isInner(int x1, int y1, int x2, int y2);

      /*! Update current mouse position.
       * \note must call SDL_PumpEvents first. */
      void updatePosition();

      /*! Update current mouse by events. Usually used in conjunction
       * with #updatePosition */
      void updateByEvent(SDL_Event event);

      /*! \return current mouse X coordinate on screen */
      int getX() { return mouseX; };
      /*! \return current mouse Y coordinate on screen */
      int getY() { return mouseY; };
      /*! \return current mouse wheel Y scrolling state */
      int getWheelState() { return mouseWheel; };
      /*! \return current mouse button state. Must be used with SDL_BUTTON. */
      Uint8 getButtonState() { return mButton; };
      /*! \return if mouse button released on last check. */
      bool buttonReleased(int buttonNumber);

      /*! \return mouse coordinate projected to 3D world */
      GLdouble getWorldX() { return xWorld; };
      /*! \return mouse coordinate projected to 3D world */
      GLdouble getWorldY() { return yWorld; };
      /*! \return mouse coordinate projected to 3D world */
      GLdouble getWorldZ() { return zWorld; };
      
      /*! \return mouse coordinate projected to world's floor (Y = 0) */
      GLdouble getFloorX() { return xFloor; };
      /*! \return mouse coordinate projected to world's floor (Y = 0) */
      GLdouble getFloorZ() { return zFloor; };

   private:
      static SingleMouseCursor* current;   /**< Active cursor */
      static std::list<SingleMouseCursor*> cursors; /**< Loaded Cursors list */

      static std::string textOver;         /**< Text over cursor (hint) */
      static GLuint textOverTexture;       /**< Texture of text over */
      static int textOverWidth;            /**< Width of text over texture */
      static int textOverHeight;           /**< Height of text over texture */
      static Uint32 textOverInit;          /**< Time inited the display */
      static std::string font;             /**< Font used to display hint */

      static Uint8 mButton;   /**< Current mouse button state */
      static int mouseX;      /**< Actual mouse X coordinates on screen */
      static int mouseY;      /**< Actual mouse Y coordinates on screen */
      static int mouseWheel;  /**< Mouse Wheel relative state */

      /*! Release state of each button*/
      static bool mouseButtonReleased[MAX_MOUSE_BUTTONS]; 

      static GLdouble xWorld; /**< Actual mouse X coordinates on World */
      static GLdouble zWorld; /**< Actual mouse Z coordinates on World */
      static GLdouble yWorld; /**< Actual mouse Y coordinates on World */

      static GLdouble xFloor; /**< Mouse floor (y=0) X intersection */
      static GLdouble zFloor; /**< Mouse floor (y=0) Z intersection */
      static Farso farso;     /**< Current farso used*/

      /*! Load Cursor file 
       * \param fileName -> file name of cursor */
      SingleMouseCursor* loadCursor(std::string fileName, int hX, int hY);

      /*! Search for a loaded cursor of fileName image */
      SingleMouseCursor* search(std::string fileName);

      /*! Update mouse world position projection */
      void updateMouseWorldPos();

      /*! Update mouse floor (y=0) projection */
      void updateMouseFloorPos();
 
};

}

#endif

