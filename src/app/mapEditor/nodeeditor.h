/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_map_editor_node_editor_h
#define _dnt_map_editor_node_editor_h

#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL.h>

#include "../../etc/scenenode.h"
#include "../../map/map.h"
#include "../../engine/character.h"
#include "../../gui/farso.h"

namespace dntMapEditor
{

enum NodeParentType
{
   NODE_PARENT_OBJECT,
   NODE_PARENT_CHARACTER,
   NODE_PARENT_DOOR
};

/*! An editor to a scene node */
class NodeEditor
{
   public:
      /*! Constructor */
      NodeEditor();

      /*! Destructor */
      ~NodeEditor();

      /*! Clear the nodeEditor (usually, after a map open) */
      void clear();

      /*! Set the current map, npc's list and clear */
      void setMap(Map* m, CharacterList* npcList);

      /*! Flush not applied changes to the map
       * \note: usually called before save the map */
      void flush();

      /*! Select a node to edit (unselecting any previous one)
       * \param scNode -> SceneNode to edit */
      void selectNode(SceneNode* scNode);

      /*! Verify and do action by mouse and state */
      void verifyAction();

      /*! Open the node window */
      void openWindow();

      /*! Treat GUI events
       * \return true if some event occurred here */
      bool eventGot(int eventInfo, Farso::GuiObject* obj);

      /*! Draw temporary elements */
      void drawTemporary();

   protected:

      /*! Delete the current node */
      void deleteCurNode();

      SceneNode* curNode;  /**< current node to edit */
      int nodeParentType; /**< Type of the node(object, character, door, etc)*/
      void* curNodeParent;  /**< Pointer to the node parent */

      Map* curMap;         /**< Current map pointer */
      CharacterList* npcs; /**< List of NPCs */

      Farso::Farso farso;  /**< interface used */
      Farso::MouseCursor mouse;
      Farso::Keyboard keyboard;
      Farso::Window* nodeWindow;  /**< Node info window */
 
      Farso::TabButton* nodeTab;     /**< Tab button for node editor */
      Farso::OneTabButton* rotX[2];  /**< Buttons for X rotation */
      Farso::OneTabButton* rotY[2];  /**< Buttons for Y rotation */
      Farso::OneTabButton* rotZ[2];  /**< Buttons for Z rotation */

      Farso::OneTabButton* moveX[2]; /**< Translate on X */
      Farso::OneTabButton* moveY[2]; /**< Translate on Y */
      Farso::OneTabButton* moveZ[2]; /**< Translate on Z */

      Farso::OneTabButton* clearRot; /**< Clear Rotation button */
      Farso::OneTabButton* deleteNode; /**< Delete Node Button */

      Farso::CxSel* gridMode;        /**< Selector if will grid mode or not */

      Farso::TextBar* deltaText;    /**< Delta Value display */
      float deltaValue;      /**< Delta to move/rotate */
};

}

#endif

