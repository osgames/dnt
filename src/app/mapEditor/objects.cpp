/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "objects.h"
using namespace std;
using namespace dntMapEditor;

#define OBJECTS_STATE_NONE    0
#define OBJECTS_STATE_ADD     1

/******************************************************
 *                      Constructor                   *
 ******************************************************/
Objects::Objects(Map* acMap)
{
   actualTool = 0;
   actualMap = acMap;
   state = OBJECTS_STATE_NONE;
   actualObstacle = NULL;
   objectFile = "";
   obstacleX = 0;
   obstacleY = 0;
   obstacleZ = 0;
   angleX = 0;
   angleY = 0;
   angleZ = 0;
   mX = -1;
   mY = -1;
}

/******************************************************
 *                       Destructor                   *
 ******************************************************/
Objects::~Objects()
{
   actualMap = NULL;
   actualObstacle = NULL;
}

/******************************************************
 *                      verifyAction                  *
 ******************************************************/
SceneNode* Objects::verifyAction(int tool)
{
   /* Avoid Input if editing text */
   if(keyboard.isEditingText())
   {
      return NULL;
   }
   const Uint8* keys = keyboard.getState();

   //FIXME -> remove the object when not insert!
   if( (tool == TOOL_OBSTACLE_ADD) && (actualObstacle != NULL))
   {
      state = OBJECTS_STATE_ADD;

      if( (mX != mouse.getX()) || (mY != mouse.getY()))
      {
         mX = mouse.getX();
         mY = mouse.getY();
         obstacleX = mouse.getWorldX();
         obstacleZ = mouse.getWorldZ();
      }
      
      /* Insert it on map */
      if(mouse.getButtonState() & SDL_BUTTON(1))
      {
         insertObject(actualMap, actualObstacle, 
                      (int)(actualObstacle->scNode->getPosX() / 
                            actualMap->squareSize()), 
                      (int)(actualObstacle->scNode->getPosZ() / 
                            actualMap->squareSize()));
         while(mouse.getButtonState() & SDL_BUTTON(1))
         {
            //Wait for Mouse Button Release
            farso.updateInputState();
         }

         /* No more with the actual */
         SceneNode* sc = actualObstacle->scNode;
         actualObstacle = NULL;
         return(sc);
      }

      /* Rotate Left/Right object */
      else if(mouse.getButtonState() & SDL_BUTTON(2))
      {
         if(keys[SDL_SCANCODE_X])
         {
            angleX += 1;
         }
         if(keys[SDL_SCANCODE_Z])
         {
            angleZ += 1;
         }
         if(keys[SDL_SCANCODE_Y])
         {
            angleY += 1;
         }
      }
      else if(mouse.getButtonState() & SDL_BUTTON(3))
      {
         if(keys[SDL_SCANCODE_X])
         {
            angleX -= 1;
         }
         if(keys[SDL_SCANCODE_Z])
         {
            angleZ -= 1;
         }
         if(keys[SDL_SCANCODE_Y])
         {
            angleY -= 1;
         }
      }

      /* Up/Down Object */
      if(keys[SDL_SCANCODE_R])
      {
         obstacleY += 0.1;
      }
      if(keys[SDL_SCANCODE_F])
      {
         obstacleY -= 0.1;
      }
      if(keys[SDL_SCANCODE_T])
      {
         obstacleY += 1.0;
      }
      if(keys[SDL_SCANCODE_G])
      {
         obstacleY -= 1.0;
      }
      if(keys[SDL_SCANCODE_0])
      {
         obstacleY = 0;
      }
      if(keys[SDL_SCANCODE_BACKSPACE])
      {
         angleX = 0;
         angleY = 0;
         angleZ = 0;
      }
      actualObstacle->scNode->set(obstacleX, obstacleY, obstacleZ,
            angleX, angleY, angleZ);
   }
   else
   {
      state = OBJECTS_STATE_NONE;
      actualObstacle = NULL;
      objectFile = "";
   }
   return(NULL);
}

/******************************************************
 *                       drawTemporary                *
 ******************************************************/
void Objects::drawTemporary()
{
}

/******************************************************************
 *                          insertObject                          *
 ******************************************************************/
void Objects::insertObject(Map* acMap, object* obj, int qx, int qz)
{
   //TODO, mark with no collision some pickable Objects
   acMap->insertObject(obj->scNode->getPosX(), 
                       obj->scNode->getPosY(),
                       obj->scNode->getPosZ(),
                       obj->scNode->getAngleX(), 
                       obj->scNode->getAngleY(),
                       obj->scNode->getAngleZ(), obj, qx, qz, 1); 
}

/******************************************************************
 *                       defineActualObject                       *
 ******************************************************************/
void Objects::defineActualObject(object* obj, string fileName)
{
   if(obj)
   {
      actualObstacle = obj;
      objectFile = fileName;
   }
}

/******************************************************************
 *                       getObjectFileName                        *
 ******************************************************************/
string Objects::getObjectFileName()
{
   return(objectFile);
}

/******************************************************************
 *                          deleteObject                          *
 ******************************************************************/
void Objects::deleteObject()
{
   if(actualObstacle)
   {
      delete(actualObstacle);
      actualObstacle = NULL;
      objectFile = "";
   }
}

