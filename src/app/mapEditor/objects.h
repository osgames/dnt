/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_map_editor_objects_h
#define _dnt_map_editor_objects_h

#include "../../map/map.h"
#include "../../etc/scene.h"
#include "../../engine/util.h"
#include "../../classes/object.h"
#include "message.h"

namespace dntMapEditor
{

/*! The objects Map Controller Class */
class Objects
{
   public:
      /*! Constructor
       * \param acMap -> poiter to opened map*/
      Objects(Map* acMap);
      /*! Destructor */
      ~Objects();

      /*! Verify and do action by mouse and state 
       * \param tool -> current Tool
       * \return pointer to object SceneNode when inserted it. */
      SceneNode* verifyAction(int tool);

      /*! If have some temporary things to draw, draw it! */
      void drawTemporary();

      /*! Define the current object to add */
      void defineActualObject(object* obj, std::string fileName);

      /*! Get object file name
       * \return fileName of object or "" if no object */
      std::string getObjectFileName();

      /*! delete actual object (if any) */
      void deleteObject();

    private:
      Map* actualMap;             /**< Actual Internal Map */
      object* actualObstacle;     /**< Actual Obstacle */
      std::string objectFile;     /**< Actual Obstacle Filename */
      int actualTool;             /**< Actual Tool */
      int state;                  /**< Actual Internal State */

      Farso::Farso farso;
      Farso::MouseCursor mouse;
      Farso::Keyboard keyboard;

      int mX, mY;

      GLfloat obstacleX,          /**< X position Obstacle */
              obstacleY,          /**< Y position Obstacle */
              obstacleZ;          /**< Z position Obstacle */
      GLfloat angleX,
              angleY,
              angleZ;

      void insertObject(Map* acMap, object* obj, int qx, int qz);

};

}

#endif

