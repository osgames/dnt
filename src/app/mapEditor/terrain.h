/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_map_editor_terrain_h
#define _dnt_map_editor_terrain_h

#include "../../map/map.h"
#include "../../engine/cursor.h"
#include "message.h"

namespace dntMapEditor
{

class Terrain
{
   public:
      /*! Constructor
       * \param acMap -> pointer to opened map */
      Terrain(Map* acMap);
      /*! Destructor */
      ~Terrain();

      /*! Verify and do action by mouse and state 
       * \param tool -> current Tool
       * \param actualTexture -> current Texture */
      void verifyAction(int tool, GLuint actualTexture);

      /*! If have some temporary things to draw, draw it! */
      void drawTemporary();

   private:
      Map* actualMap;   /**< Actual Internal Map */
      int actualTool;   /**< Actual Tool */
      int state;        /**< Actual Internal State */

      int quadX;        /**< Mouse Mouse Square X Coordinate */
      int quadZ;        /**< Mouse Mouse Square Z Coordinate */

      int quadInitX;    /**< X quad value at init of some state */
      int quadInitZ;    /**< Y quad value at init of some state */

      GLfloat height;      /**< Current Map Height On Mouse */
      GLfloat nivelHeight; /**< Map Height at init of Nivelate */

      GLfloat mX, mY, mZ, fX, fZ;  /**< Mouse Positions */
 
      GLfloat initmX, initmZ; /**< Mouse Positions On Init of State */

      DNT::Mouse mouse;

      /*! Do up Down on Terrain */
      void doUpDown();
      /*! Do Nivelate on Terrain */
      void doNivelate();
      /*! Do Change Texture on Terrain */
      void doTexture(GLuint actualTexture );
      /*! Verifies if point is in Square */
      bool pointInSquare(GLfloat x, GLfloat y, 
                         GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2);
};

}

#endif

