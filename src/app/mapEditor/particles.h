/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_map_editor_particles_h
#define _dnt_map_editor_particles_h

#include "grasswindow.h"
#include "waterwindow.h"
#include "message.h"
#include "guiIO.h"
#include "../../map/map.h"
#include "../../particle/partcontroller.h"

namespace dntMapEditor
{

class Particles
{
   public:
      /*! Constructor
       * \param acMap -> poiter to opened map*/
      Particles(Map* acMap);
      /*! Destructor */
      ~Particles();

      /*! Verify and do action by mouse and state 
       * \param grWindow -> the grassWindow Used*/
      void verifyAction(GuiIO* gui, partController* pS,
                        std::string selectedText, GrassWindow* grWindow,
                        WaterWindow* wtWindow, Map* actualMap);

      /*! If have some temporary things to draw, draw it! */
      void drawTemporary();

      void deleteParticle();

   private:
      Map* actualMap;                  /**< actual Opened Map */
      particleSystem* actualParticle;  /**< actual Particle */
      int state;                       /**< actual State */
      int particleType;                /**< actual Particle Type */
      GLfloat height;                  /**< actual Particle Height */
      Farso::Farso farso;
      Farso::Keyboard keyboard;
      Farso::MouseCursor mouse;

      std::string previousText;        /**< Previous selectedText */

      GLfloat x1,                      /**< X1 coordinate for grass */
              z1,                      /**< Z1 Coordinate for grass */
              x2,                      /**< X2 coordinate for grass */
              z2;                      /**< Z2 coordinate for grass */

      //interPlane* actualPlane;         /**< actual Plane */
};

}

#endif

