/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "npcs.h"

#include "../../engine/util.h"

#define NPCS_STATE_NONE    0
#define NPCS_STATE_ADD     1

using namespace std;
using namespace dntMapEditor;

/******************************************************
 *                      Constructor                   *
 ******************************************************/
Npcs::Npcs(Map* acMap, CharacterList* NPCsList, featsList* lFeats)
{
   actualMap = acMap;
   state = NPCS_STATE_NONE;
   actualNpc = NULL;
   npcFile = "";
   intList = new(CharacterList);
   NPCs = NPCsList;
   features = lFeats;
   mX = -1;
   mY = -1;
}

/******************************************************
 *                       Destructor                   *
 ******************************************************/
Npcs::~Npcs()
{
   actualMap = NULL;
}

/******************************************************
 *                      verifyAction                  *
 ******************************************************/
void Npcs::verifyAction(int tool)
{
   if(actualNpc == NULL)
   {
      return;
   }

   if(tool == TOOL_NPC_ADD)
   {
      state = NPCS_STATE_ADD;

      /* Changed Character Position */
      if( (mX != mouse.getX()) || (mY != mouse.getY()))
      {
         /* Set the new position */
         mX = mouse.getX();
         mY = mouse.getY();
         actualNpc->scNode->setPosition(mouse.getWorldX(),
               actualMap->getHeight(mouse.getWorldX(), mouse.getWorldZ()),
               mouse.getWorldZ()); 
      }

      /* Rotate the NPC + */
      if( (mouse.getButtonState() & SDL_BUTTON(2)) && (actualNpc != NULL) )
      {
         actualNpc->scNode->setAngle(0.0f, 
               ((int)(actualNpc->scNode->getAngleY()+1))%360,0.0f);
      }

      /* Rotate the NPC - */
      if( (mouse.getButtonState() & SDL_BUTTON(3)) && (actualNpc != NULL) )
      {
         actualNpc->scNode->setAngle(0.0f, 
               ((int)(actualNpc->scNode->getAngleY()-1))%360,0.0f);
      }
     
      /* Insert the NPC */
      if( (mouse.getButtonState() & SDL_BUTTON(1)) && (actualNpc != NULL) )
      {
         insertNpc(mouse.getWorldX(), mouse.getWorldZ(), actualNpc, 
                   (int)(mouse.getWorldX() / actualMap->squareSize()), 
                   (int)(mouse.getWorldZ() / actualMap->squareSize()));
         while(mouse.getButtonState() & SDL_BUTTON(1))
         {
            //Wait for Mouse Button Release
            farso.updateInputState();
         }
      }
   }
   else
   {
      state = NPCS_STATE_NONE;
   }
}

/******************************************************************
 *                         drawTemporary                          *
 ******************************************************************/
void Npcs::drawTemporary()
{
}

/******************************************************************
 *                           insertNpc                            *
 ******************************************************************/
void Npcs::insertNpc(GLfloat xReal, GLfloat zReal,
                     Character* npc, int qx, int qz)
{
   Character* per;
   per = NPCs->insertCharacter(npcFile,features, NULL, "");
   per->scNode->set(xReal, actualMap->getHeight(xReal, zReal), zReal,
      0.0f, npc->scNode->getAngleY(), 0.0f);
}

/******************************************************************
 *                        defineActualNpc                         *
 ******************************************************************/
void Npcs::defineActualNpc(string fileName)
{
   if(npcFile != fileName)
   {
      delete(intList);
      intList = new(CharacterList);
      actualNpc = intList->insertCharacter(fileName, features, NULL, "");
      npcFile = fileName;
   }
}

/******************************************************************
 *                            deleteNpcs                          *
 ******************************************************************/
void Npcs::deleteNpc()
{
   if(actualNpc)
   {
      intList->removeCharacter(actualNpc);
      npcFile = "";
      actualNpc = NULL;
   }
}

/******************************************************************
 *                       getObjectFileName                        *
 ******************************************************************/
string Npcs::getNpcFileName()
{
   return(npcFile);
}

/******************************************************************
 *                             saveFile                           *
 ******************************************************************/
bool Npcs::saveFile(string fileName)
{
   FILE* arq;
   string saveName;

   if(!(arq = fopen(fileName.c_str(),"w")))
   {
      return(false);
   }
   else
   {
      int npc;
      fprintf(arq,"%d\n",NPCs->getTotal());
      Character* per = (Character*) NPCs->getFirst();
      for(npc = 0; npc < NPCs->getTotal(); npc++)
      {
         saveName = replaceSpaces(per->name);
         fprintf(arq,"%s %s %.3f %.3f %.3f %d\n", saveName.c_str(),
                 per->getCharacterFile().c_str(),
                 per->scNode->getPosX(),
                 per->scNode->getPosZ(), 
                 per->scNode->getAngleY(),
                 per->getPsychoState());
         per = (Character*) per->getNext();
       }
       fclose(arq);
   }
   return(true);
}

