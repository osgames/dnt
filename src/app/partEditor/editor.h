/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_part_editor_h
#define _dnt_part_editor_h

#include "../../gui/farso.h"
#include "../../particle/particle.h"
#include "../../etc/dirs.h"
#include "../../engine/camera.h"

#include "partaux.h"
#include "partelementwindow.h"
#include "partwindow.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#ifdef __APPLE__
   #include <SDL_image.h>
#else
   #include <SDL2/SDL_image.h>
#endif

/*! The partEditor main class */
class editor
{
   public:
      /*! Constructor */ 
      editor();
      /*! Destructor */
      ~editor();
      
      /*! Run the editor */
      void run();


   protected:

      /*! Create all Windows */
      void createWindows();

      /*! Treat GUI Related Events */
      void treatGuiEvents();

      /*! Create a Particle to edit
       * \return -> true if can create, false otherwise */
      bool createParticle();

      /*! Delete the current particle */
      void deleteParticle();

      /*! Update the particle */
      void updateParticle();

      /*! Render things */
      void render();

      int type;    /**< Current particle type */
      partAux* p;  /**< The Current Particle */

      bool done;   /**< Flux Controll Variable */

      Farso::Farso farso; /**< Farso interface used */

      dirs dir;            /**< Directories Definition */
      extensions ext;      /**< OpenGL Extensions */

      camera* gameCamera; /**< The OpenGL Camera Used */

      Farso::Button* buttonLoad; /**< Load Button */
      Farso::Button* buttonSave; /**< Save Button */
      Farso::Button* buttonExit; /**< Exit Button */

      Farso::Window* actWindow;
      partWindow* particleWindow;        /**< Window to edit particle */

      /**********************File*Window*******************************/
      Farso::Window* fileWindow;  /**< File Selector Window (for load/save) */
      Farso::FileSel* fileSelector;   /**< The file selector itself */
      bool fileLoading;        /**< If is loading or saving */
      void openFileWindow(bool load);

      Farso::MouseCursor mouse;
      Farso::Keyboard keyboard;

      std::string curFileName;      /**< Filename of Current Particle Open */
};

#endif

