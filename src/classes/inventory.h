/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_Inventory_h
#define _dnt_Inventory_h

#include "object.h"
#include "itemslot.h"
#include "money.h"
#include "../etc/dirs.h"
#include "../map/map.h"

#include <SDL2/SDL.h>
#ifdef __APPLE__
   #include <SDL_image/SDL_image.h>
#else
   #include <SDL2/SDL_image.h>
#endif

#define INVENTORY_SIZE_X 13 /**< Number of Spaces on X axis */
#define INVENTORY_SIZE_Y 6  /**< Number of Spaces on Y axis */

#define TRADE_SIZE_X     6  /**< Number of Spaces on X axis for Trades */
#define TRADE_SIZE_Y    10  /**< Number of Spaces on Y axis for Trades */

#define INVENTORY_TOTAL_PLACES  8  /**< Total Eqquiped Places */
#define INVENTORY_HEAD          0  /**< Head Place */
#define INVENTORY_LEFT_HAND     1  /**< Left Hand Place */
#define INVENTORY_RIGHT_HAND    2  /**< Right Hand Place */
#define INVENTORY_LEFT_FINGER   3  /**< Left Finger Place */
#define INVENTORY_RIGHT_FINGER  4  /**< Right Finger Place */
#define INVENTORY_NECK          5  /**< Neck Place */
#define INVENTORY_FOOT          6  /**< Foot Place */
#define INVENTORY_BODY          7  /**< Body Place (Torso) */

#define INVENTORY_INVENTORY     8  /**< is in Inventory */

#define INVENTORY_PER_CHARACTER 4  /**< Number of Inventories per Character */

class InventWindow;

/*! Character Inventory Definition */
class Inventory
{
   public:
      /*! Constructor */
      Inventory();
      /*! Destructor */
      ~Inventory();

      /*! Add Money to the Inventory
       * \param qty -> money quantity (value) 
       * \return true if could add, false otherwise */
      bool addMoney(int qty);

      /*! Get money quantity */
      int getMoneyQuantity();

      /*! Get money object at Inventory, if any */
      money* getMoney();

      /*! Dec money value from current money object
       * \note -> if remaining quantity is == 0, object is deleted. 
       * \return true if could subtract, false if quantity insuficient */
      bool decMoney(int qty);

      /*! Add object to Inventory space
       * \param obj -> pointer to object to be added
       * \param x -> x space position to be the orign on Inventory
       * \param y -> y space position to be the orign on Inventory
       * \param curInv -> current Inventory
       * \return -> true if added to Inventory, false if can't add. */
      bool addObject(object* obj, int x, int y, int curInv);

      /*! Add object to first free Inventory space
       * \param obj -> pointer to object to be added
       * \return -> true if added to Inventory, false if can't add. */
      bool addObject(object* obj);

      /*! Equip Object to the Place defined.
       * \param obj -> pointer to object to be equipped
       * \param where -> ID of the place to equip
       * \return -> true if equipped, false, if can't equip */
      bool equipObject(object* obj, int where);

      /* Drop object from the Inventory (puting it on the floor X,Z)
       * \param obj -> object to drop from the Inventory
       * \param x -> x position of the object on Inventory
       * \param y -> y position of the object at Inventory
       * \param inv -> Inventory where object lies
       * \param actualMap -> pointer to the current map
       * \param X -> x position to drop object to
       * \param Z -> z position to drop object to */
      void dropObject(object* obj, int x, int y, int inv,
            Map* actualMap, GLfloat X, GLfloat Z);

      /*! Get object from iventory position
       * \param x -> x space position on Inventory
       * \param y -> y space position on Inventory
       * \param curInv -> current Inventory
       * \return -> pointer to object on position */
      object* getFromPosition(int x, int y, int curInv);

      /*! Get object from Inventory place
       * \param where -> place ID on Inventory
       * \return -> pointer to object on position */
      object* getFromPlace(int where);

      /*! Get the first object founded on the Inventory. 
       * \param x -> x position of the item got
       * \param y -> y position of the item got
       * \param curInv -> Inventory number to get object from
       * \return -> pointer to the first founded item, or NULL. */
      object* getFirstObject(int& x, int& y, int curInv);

      /*! Get next object on Inventory.
       * \note -> this function is usually called after a getFirstObject,
       *          with the result x,y coordinate got from that 
       * \param x -> x coordinate of the previous item got (will have 
       *             the x coordinate of the current after the function)
       * \param y -> y coordinate of the previos item got (will have the
       *             y coordinate of the current after the function)
       * \param curInv -> Inventory number to get object from
       * \return -> pointer to the object found or NULL, if no more objects */
      object* getNextObject(int& x, int& y, int curInv);

      /*! Remove object from Inventory
       * \param obj -> pointer to object to be removed */
      void removeFromInventory(object* obj);

      /*! Remove object from Inventory
       * \param x -> x space position on Inventory
       * \param y -> y space position on Inventory 
       * \param curInv -> current Inventory */
      void removeFromInventory(int x, int y, int curInv);

      /*! Remove Object from Equipped Place
       * \param where -> equipped place ID */
      void removeFromPlace(int where);

      /*! Verify if can Add object to Inventory position
       * \param obj -> pointer to object to be added
       * \param x -> x Inventory position
       * \param y -> y Inventory position
       * \param curInv -> current Inventory
       * \return -> true if can add, false otherwise */
      bool canAdd(object* obj, int x, int y, int curInv);

      /*! Draw Inventory to Surface
       * \param surface -> SDL_Surface to draw the iventory 
       * \param x -> x coordinate on surface
       * \param y -> y coordinate on surface
       * \param curInv -> current Inventory */
      void draw(int x, int y, SDL_Surface* surface, int curInv);

      /*! Draw Equiped Itens to Surface
       * \param surface -> SDL_Surface to draw the iventory 
       * \param x -> x coordinate on surface
       * \param y -> y coordinate on surface */
      void drawEquiped(int x, int y, SDL_Surface* surface);

      /*! Get a item from the Inventory with the desired fileName, if exists.
       * \param fileName -> fileName of the item
       * \return -> pointer to the found object of NULL */
      object* getItemByFileName(std::string fileName);

      /*! Get a item from the Inventory with the desired relatedInfo, if exists.
       * \param relatedInfo -> info of the item
       * \return -> pointer to the found object of NULL */
      object* getItemByInfo(std::string relatedInfo);

      /*! Count the items with relatedInfo
       * \relatedInfo -> info of the item to count
       * \return -> number of items with relatedInfo */
      int countItemByInfo(std::string relatedInfo);

      /*! Set the Inventory Opened Window
       * \param w -> pointer to an InventWindow */
      void setOpenedWindow(InventWindow* w);

      /*! Get the current Inventory opened window
       * \return -> pointer to the current Inventory window or NULL */
      InventWindow* getOpenedWindow();

      /*! Get total number of items at the Inventory
       * \return -> total number of items */
      int getTotalItems();

   private:
      /*! For debug: print all names of Inventory itens on terminal */
      void print();

      InventWindow* openedWindow;          /**< Pointer to the opened window */

      ItemSlot** slots;            /**< The Inventory */
      ItemSlot** equippedSlots;    /**< The Equipped Slots */

      SDL_Surface* InventoryImage; /**< The Inventory Image */
      SDL_Surface* equipedImage;   /**< The Equiped Inventory Image */

};

#endif

