/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _dnt_comicbook_h
#define _dnt_comicbook_h

#include <SDL2/SDL.h>

#include "comicpage.h"
#include "comicbox.h"
#include "../gui/farso.h"
#include "../engine/cursor.h"

/*! The comic book class is the DNT cutscene abstraction */
class ComicBook
{
   public:
      /*! Constructor */
      ComicBook();
      /*! Destructor */
      ~ComicBook();

      /*! Load a comicBook from file
       * \param bookFile -> comicBook file name 
       * \return -> true if the load was successfull */
      bool load(std::string bookFile);

      /*! Run the comic book */
      void run();

   protected:

      /*! Render the current page (with or without a scale effect)
       * \param curPage -> current page to render
       * \param scale -> scale factor to use */
      void render(ComicPage* curPage, float scale=1.0);

      /*! Insert a page at the book pages list
       * \param t -> page title 
       * \return -> pointer to the comic page created */
      ComicPage* insertPage(std::string t);

      /*! Empty the book, deleting all its pages */
      void empty();

      /*! Verify User Input to the comic (skip comic or box for example)
       * \return -> true if is to skip current comicBox */
      bool verifyInput();

      GLuint skipTexture; /**< The Skip Texture */
      float skipScale;    /**< The skip scale factor */
      float skipSum;      /**< The sum to the skip scale */

      bool exit;          /**< True if pressed skip button o ESC key */
      bool changeColor;   /**< True to change skip texture color */

      DntList pages;/**< Each comic book page  */
      std::string title;  /**< Book Title */
      Farso::Farso farso; /**< Farso interface */
      DNT::Mouse mouse;   /**< Current mouse used */
      Farso::Keyboard keyboard;  /**< Current keyboard */
};

#endif

