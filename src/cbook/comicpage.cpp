/* 
  DNT: a satirical post-apocalyptical RPG.
  Copyright (C) DNTeam <dnt@dnteam.org>
 
  This file is part of DNT.
 
  DNT is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DNT is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DNT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "comicpage.h"
#include "../etc/dirs.h"
#include "../gui/dntfont.h"
#include "../gui/draw.h"
#include "../engine/util.h"

#include <iostream>

#ifdef __APPLE__
   #include <SDL_image/SDL_image.h>
#else
   #include <SDL2/SDL_image.h>
#endif

using namespace std;

/***********************************************************************
 *                             Constructor                             *
 ***********************************************************************/
ComicPage::ComicPage()
{
   boxes = DntList(DNT_LIST_TYPE_ADD_AT_END);
   texture = NULL;
   glGenTextures(1, &tex);
}

/***********************************************************************
 *                              Destructor                             *
 ***********************************************************************/
ComicPage::~ComicPage()
{
   /* Free texture */
   glDeleteTextures(1,&tex);
   if(texture != NULL)
   {
      SDL_FreeSurface(texture);
   }
}

/***********************************************************************
 *                            flushTexture                             *
 ***********************************************************************/
void ComicPage::flushTexture()
{
   if(texture != NULL)
   {
      Farso::setTexture(texture, tex);
   }
}

/***********************************************************************
 *                             getFirstBox                             *
 ***********************************************************************/
ComicBox* ComicPage::getFirstBox()
{
   return((ComicBox*)boxes.getFirst());
}

/***********************************************************************
 *                            getTotalBoxes                            *
 ***********************************************************************/
int ComicPage::getTotalBoxes()
{
   return(boxes.getTotal());
}

/***********************************************************************
 *                               getWidth                              *
 ***********************************************************************/
int ComicPage::getWidth()
{
   if(texture)
   {
      return(texture->w);
   }
   return(0);
}

/***********************************************************************
 *                               getHeight                             *
 ***********************************************************************/
int ComicPage::getHeight()
{
   if(texture)
   {
      return(texture->h);
   }
   return(0);
}

/***********************************************************************
 *                               render                                *
 ***********************************************************************/
void ComicPage::render()
{
   int i;
   ComicBox* box = (ComicBox*)boxes.getFirst();

   /* Calculate scale ratio */
   GLfloat ratio = 1.0f;
   if(texture)
   { 
      ratio = (float)Farso::SCREEN_Y / (float)texture->h;
   }

   glColor4f(1.0f,1.0f,1.0f,1.0f);

   glPushMatrix();
   glDisable(GL_DEPTH_TEST);

   /* Center the Page on screen */
   if(texture)
   {
      glTranslatef(((Farso::SCREEN_X/2.0) - ratio*(texture->w/2.0)),0.0,0.0);

      /* Render Blank Page */
      GLfloat midX = ratio * texture->w;
      GLfloat midY = ratio * texture->h;
      glBegin(GL_QUADS);
         glVertex2f(0, 0);
         glVertex2f(midX, 0);
         glVertex2f(midX, midY);
         glVertex2f(0, midY);
      glEnd();

   }

   /* Render Page Boxes */
   glEnable(GL_TEXTURE_2D);
   glBindTexture(GL_TEXTURE_2D, tex);
   for(i = 0; i < boxes.getTotal(); i++)
   {
      if(box->getStatus() != COMIC_BOX_STATUS_INACTIVE)
      {
         box->render();
      }
      box = (ComicBox*)box->getNext();
   }
   glPopMatrix();

   glDisable(GL_TEXTURE_2D);
   glEnable(GL_DEPTH_TEST);
}

/***********************************************************************
 *                              insertBox                              *
 ***********************************************************************/
void ComicPage::insertBox(ComicBox* box)
{
   boxes.insert(box);
}


/***********************************************************************
 *                             insertText                              *
 ***********************************************************************/
void ComicPage::insertText(int x1, int y1, int x2, int y2, string text)
{
   insertText(x1, y1, x2, y2, text, 16, Farso::Font::STYLE_NORMAL, 0, 0, 0);
}

/***********************************************************************
 *                             insertText                              *
 ***********************************************************************/
void ComicPage::insertText(int x1, int y1, int x2, int y2, string text,
                           int fontSize, int fontStyle, 
                           int R, int G, int B)
{
   if(texture != NULL)
   {
      Farso::Font fnt;
      dirs dir;
      fnt.defineFont(dir.getRealFile(DNT_FONT_TIMES), fontSize);
      fnt.defineFontAlign(Farso::Font::ALIGN_CENTER);
      fnt.defineFontStyle(fontStyle);

      Farso::color_Set(R, G, B, 255);
      fnt.write(texture, x1+1,y1+2, text, x1,y1,x2,y2);

      fnt.defineFontStyle(Farso::Font::STYLE_NORMAL);
   }
   else
   {
      cerr << "Warning: tried to define comic text '" << text 
           << "' without texture!" << endl;
   }
}


/***********************************************************************
 *                          defineTexture                              *
 ***********************************************************************/
bool ComicPage::defineTexture(string textureFile)
{
   dirs dir;
   if(texture != NULL)
   {
      SDL_FreeSurface(texture);
   }

   texture = IMG_Load(dir.getRealFile(textureFile).c_str());

   if(!texture)
   {
      cerr << "Error on loading image: '" << textureFile << "'" << endl;
      return(false);
   }

   return(true);
}

